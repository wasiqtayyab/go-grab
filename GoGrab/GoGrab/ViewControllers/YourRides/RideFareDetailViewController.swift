//
//  RideFareDetailViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 26/05/2021.
//

import UIKit

class RideFareDetailViewController: UIViewController {

    @IBOutlet weak var imgUpDown: UIImageView!
    @IBOutlet weak var viewTopTap: UIView!
    @IBOutlet weak var viewCenter: UIView!
    @IBOutlet weak var contHeightCentrView: NSLayoutConstraint!
    // MARK:  Description
    
    var isExpanded = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewCenter.isHidden = true
        self.contHeightCentrView.constant = 0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchTapped(_:)))
        
        self.viewTopTap.addGestureRecognizer(tap)
    }
    @objc func touchTapped(_ sender: UITapGestureRecognizer) {
        if isExpanded == false {
            UIView.animate(withDuration: 0.4) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.viewCenter.isHidden = false
                }
                self.contHeightCentrView.constant = 272
                self.imgUpDown.image = UIImage(named: "chevronDown")
                
                self.view.layoutIfNeeded()
            }
            self.isExpanded = true
        }else {
            UIView.animate(withDuration: 0.4) {
              
                self.contHeightCentrView.constant = 0
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.viewCenter.isHidden = true
                }
                self.imgUpDown.image = UIImage(named: "chevronUp")
                self.view.layoutIfNeeded()
            }
            self.isExpanded = false
        }
    }

    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
