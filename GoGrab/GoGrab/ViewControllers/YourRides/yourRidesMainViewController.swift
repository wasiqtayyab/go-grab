//
//  yourRidesMainViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit
import XLPagerTabStrip

class yourRidesMainViewController: ButtonBarPagerTabStripViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        buttonBarView.selectedBar.backgroundColor = #colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1)
        
        settings.style.buttonBarItemFont = UIFont(name: "AirbnbCerealApp-Medium", size: 17.0) ?? .systemFont(ofSize: 17.0)
      //  settings.style.buttonBarItemTitleColor = .black
       
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
   
       
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            newCell?.label.textColor = #colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1)
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
//        let child_1 = TableChildExampleViewController(style: .plain, itemInfo: IndicatorInfo(title: "FRIENDS"))
//        let child_1 = favVideosViewController(itemInfo: "Videos")

        
        let child1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "yourRidesDetailsViewController") as! yourRidesDetailsViewController
        child1.itemInfo = "Schedule"

        let child2 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "yourRidesDetailsViewController") as! yourRidesDetailsViewController
        child2.itemInfo = "History"
        

        return [child1,child2]
    }
    @IBAction func backPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
 

}
