//
//  yourRidesDetailsViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit
import XLPagerTabStrip

class yourRidesDetailsViewController: UIViewController,IndicatorInfoProvider {
    var itemInfo:IndicatorInfo = "View"

    @IBOutlet weak var viewCustomers: UIView!
    @IBOutlet weak var viewSuppliers: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("title info: ",itemInfo.title)

//        lbl.text = itemInfo.title
        
        if itemInfo.title == "Schedule"{
            print("Schedule")
            viewSuppliers.isHidden = true
            viewCustomers.isHidden = false
        } else if itemInfo.title == "History"{
            print("History")
            viewSuppliers.isHidden = false
            viewCustomers.isHidden = true
        }

        
        // Do any additional setup after loading the view.
    }
    

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        print(itemInfo)
        return itemInfo
    }

}
