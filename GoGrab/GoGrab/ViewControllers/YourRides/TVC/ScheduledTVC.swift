//
//  ScheduledTVC.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit

class ScheduledTVC: UITableViewCell {

    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPickUpLoc: UILabel!
    @IBOutlet weak var lblDropOffLoc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
