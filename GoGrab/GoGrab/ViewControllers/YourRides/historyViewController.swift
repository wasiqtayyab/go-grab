//
//  historyViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit

class historyViewController: UIViewController {
    @IBOutlet weak var tblHistory: UITableView!
    
    @IBOutlet weak var viewWhoops: UIView!
    @IBOutlet weak var lblWhoops: UILabel!
    
    var arrHistory = [["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],]
    override func viewDidLoad() {
        super.viewDidLoad()

        tblHistory.delegate = self
        tblHistory.dataSource = self
        lblWhoops.text = "You don't have any \nrides history."
    }

}
extension historyViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduledTVC", for: indexPath) as! ScheduledTVC
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RideFareDetailViewController") as! RideFareDetailViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
