//
//  scheduleViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit

class scheduleViewController: UIViewController {
    @IBOutlet weak var tblSchedule: UITableView!
    
    
    @IBOutlet weak var viewWhoops: UIView!
    @IBOutlet weak var lblWhoops: UILabel!
    
    var arrSchedule = [["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],
                       ["dateTime":"Yesterday, 2:39 PM","price":"$29","pickUp":"Venesse road - NYC - New York","dropOff":"27 West 4th Street, New York, NY 10012"],]
    override func viewDidLoad() {
        super.viewDidLoad()

        tblSchedule.delegate = self
        tblSchedule.dataSource = self
        self.tblSchedule.isHidden = true
        
        lblWhoops.text = "You don't have any \nrides planned."
    }
    

    


}
extension scheduleViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSchedule.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduledTVC", for: indexPath) as! ScheduledTVC
        return cell
    }
    
    
}
