//
//  AddToSvedPlacesViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit

class AddToSvedPlacesViewController: UIViewController {
    @IBOutlet weak var viewTopNav: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    
        self.viewTopNav.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.viewTopNav.layer.shadowRadius = 10
        self.viewTopNav.layer.shadowOpacity = 0.4
        self.viewTopNav.layer.shadowOffset = CGSize.zero
    }
    

    @IBAction func addToSavePressed(_ sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
        
    }
   

}
