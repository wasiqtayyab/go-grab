//
//  EmailOtpViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 06/08/2021.
//

import UIKit
import DPOTPView

class EmailOtpViewController: UIViewController {

    //MARK:- Outlet
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var otpView: DPOTPView!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    
    //MARK:- View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.phoneNumberLabel.text = "We sent a code to \(email).Enter the code in that message."
        
        otpView.dpOTPViewDelegate = self
        
    }
    
    //MARK:- Button Action
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func tryAgainButtonPressed(_ sender: UIButton) {
        
        otpView.text = ""
        otpView.becomeFirstResponder()
        forgetPasswordApi()
    }
    
    //MARK:- Verify Forget Password Api
    
    private func verifyForgetPasswordApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyforgotPasswordCode(email: email, code: otpView.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordViewController")as! ResetPasswordViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                        
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
    //MARK:- Forget Password Api
    
    private func forgetPasswordApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.forgotPassword(email: email) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                   
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}


//MARK:- Textfield Delegate

extension EmailOtpViewController: DPOTPViewDelegate {
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        if position == 3 && text.count == 4 {
            otpView.dismissOnLastEntry = true
            verifyForgetPasswordApi()
        }
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {

    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
        
    }
    
    
}
 


