//
//  LoginViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 06/07/2021.
//

import UIKit
import CocoaTextField
import PhoneNumberKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices
import KeychainAccess

class LoginViewController: UIViewController, UITextFieldDelegate,GIDSignInDelegate,ASAuthorizationControllerPresentationContextProviding{
    
    //MARK:- OUTLET
    
    @IBOutlet weak var viewForTextField: UIView!
    @IBOutlet weak var tfCountryRegion: CocoaTextField!
    @IBOutlet weak var tfPhoneNumber: CocoaTextField!
    @IBOutlet weak var tfEmail: CocoaTextField!
    
    @IBOutlet weak var countryBorderView: UIView!
    @IBOutlet weak var phoneBorderView: UIView!
    @IBOutlet weak var emailBorderView: UIView!
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnApple: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var callLabel: UILabel!
    
    @IBOutlet weak var continueButtonConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var callLabelViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emailViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainTextFieldView: NSLayoutConstraint!
    
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
        
   
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailBorderView.isHidden = true
        emailViewConstraint.constant = 0
        continueButtonConstraint.constant = 213
        
        configureButton()
        configureViewTextField()
        configurePhoneTextField()
        configureCountryTextField()
        
      
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.tfCountryRegion.text = country_code
       
    }
    
  
    //MARK:- Configure
    
    private func configureEmailTextField(){
        
        tfEmail.delegate = self
        self.tfEmail.placeholder = "Email"
        self.tfEmail.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.borderWidth = 1.5
        self.tfEmail.defaultBackgroundColor = .clear
        self.tfEmail.focusedBackgroundColor = .clear
        self.tfEmail.cornerRadius = 7
        self.tfEmail.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
    }
    
    private func configureCountryTextField(){
        
        tfCountryRegion.delegate = self
        self.tfCountryRegion.placeholder = "Country/Region"
        self.tfCountryRegion.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfCountryRegion.borderWidth = 1.5
        self.tfCountryRegion.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        self.tfCountryRegion.defaultBackgroundColor = .clear
        self.tfCountryRegion.focusedBackgroundColor = .clear
        self.tfCountryRegion.cornerRadius = 7
        self.tfCountryRegion.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfCountryRegion.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }
    
    private func configurePhoneTextField(){
        
        self.tfPhoneNumber.delegate = self
        self.tfPhoneNumber.placeholder = "Enter Phone Number"
        self.tfPhoneNumber.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPhoneNumber.borderWidth = 1.5
        self.tfPhoneNumber.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        self.tfPhoneNumber.defaultBackgroundColor = .clear
        self.tfPhoneNumber.focusedBackgroundColor = .clear
        self.tfPhoneNumber.cornerRadius = 7
        self.tfPhoneNumber.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPhoneNumber.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        
    }
    
    private func configureViewTextField(){
        
        self.viewForTextField.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.viewForTextField.layer.borderWidth = 1
        self.viewForTextField.layer.cornerRadius = 7
        
    }
    
    private func configureButton(){
        
        btnEmail.layer.borderWidth = 2
        btnEmail.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        btnApple.layer.borderWidth = 2
        btnApple.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        btnGoogle.layer.borderWidth = 2
        btnGoogle.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        btnFacebook.layer.borderWidth = 2
        btnFacebook.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    
    //MARK:- TEXTFIELD ACTION
    
    @IBAction func countryDidBegin(_ sender: UITextField) {
        
        if sender == tfCountryRegion {
            countryBorderView.layer.borderWidth = 3
            countryBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            countryBorderView.layer.cornerRadius = 7
            countryBorderView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            
        }else if sender == tfPhoneNumber {
            
            phoneBorderView.layer.borderWidth = 3
            phoneBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            phoneBorderView.layer.cornerRadius = 7
            phoneBorderView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            
        }else {
            emailBorderView.layer.borderWidth = 3
            emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            emailBorderView.layer.cornerRadius = 7
            
        }
        
    }
    
    @IBAction func countryDidEnd(_ sender: UITextField) {
        if sender == tfCountryRegion{
            countryBorderView.layer.borderWidth = 0
            countryBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            countryBorderView.layer.cornerRadius = 7
            countryBorderView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        }else if sender == tfPhoneNumber {
            phoneBorderView.layer.borderWidth = 0
            phoneBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            phoneBorderView.layer.cornerRadius = 7
            phoneBorderView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        }
        else {
            emailBorderView.layer.borderWidth = 0
            emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            emailBorderView.layer.cornerRadius = 7
            
        }
        
    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func continueWithApple(_ sender: UIButton) {
        self.appleDelegate()
    }
    
    
    @IBAction func continueWithGoogle(_ sender: UIButton) {
        self.googleDelegate()
    }
    
    
    @IBAction func continueWithFacebook(_ sender: UIButton) {
        self.loginFacebook()
    }
    
    
    
    @IBAction func countryButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CountryViewController")as! CountryViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func continueButtonPressed(_ sender: UIButton){
        
        if isEmail == true {
            
            if AppUtility!.isEmpty(tfCountryRegion.text!){
                AppUtility?.showToast(string: "Enter a valid country region", view: self.view)
                
                return
            }
            
            if !AppUtility!.isValidPhoneNumber(strPhone: tfPhoneNumber.text!){
                AppUtility?.showToast(string: "Enter a valid phone number", view: self.view)
                return
            }
        
            let trimmedString = AppUtility?.validate(tfPhoneNumber.text!)
            
            let index = trimmedString!.index(trimmedString!.startIndex, offsetBy: 0)
            String(trimmedString![index])
            print("print", String(trimmedString![index]) )
            if String(trimmedString![index]) == "+"{
                phone_Number = trimmedString!
            }else{
                phone_Number = "+92\((trimmedString)!)"
            }
            
            self.phoneNumberApi()
            
        }else {
            
            if !AppUtility!.isEmail(tfEmail.text!){
                AppUtility?.showToast(string: "Enter a valid email", view: self.view)
                return
            }
            
            email = self.tfEmail.text!
            self.registerEmailApi()
            
        }
        
    }
    
    
    @IBAction func continueWithEmailButtonPressed(_ sender: UIButton) {
        btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
        
        if isEmail == true {
            
            btnEmail.setTitle("Continue with Phone", for: .normal)
            emailBorderView.isHidden = false
            emailViewConstraint.constant = 60
            emailImage.image = UIImage(named: "81-81")
            viewForTextField.isHidden = true
            textFieldViewConstraint.constant = 0
            mainTextFieldView.constant = 260  //260
            callLabel.isHidden = true
            callLabelViewConstraint.constant = 0
            continueButtonConstraint.constant = 65  //65
            configureEmailTextField()
            tfEmail.text = ""
            isEmail = false
            
        }else {
            
            btnEmail.setTitle("Continue with Email", for: .normal)
            emailBorderView.isHidden = true
            emailViewConstraint.constant = 0
            emailImage.image = UIImage(named: "4-4")
            viewForTextField.isHidden = false
            textFieldViewConstraint.constant = 119
            mainTextFieldView.constant = 350
            callLabel.isHidden = false
            callLabelViewConstraint.constant = 34
            continueButtonConstraint.constant = 213
            tfPhoneNumber.text = ""
            
            isEmail = true
            
        }
    }
    
    //MARK:- TEXTFIELD DELEGATES
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if isEmail == true {
            
            if self.tfPhoneNumber.text != "" && self.tfCountryRegion.text != "" {
                
                if !AppUtility!.isValidPhoneNumber(strPhone: self.tfPhoneNumber.text!){
                    
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                
                btnContinue.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
            }else {
                btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            }
            
            
        }else {
            
            if self.tfEmail.text != "" {
                
                if !AppUtility!.isEmail(tfEmail.text!){
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                btnContinue.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
            }else {
                
                btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            }
            
        }
    }
    
    
    //MARK:- PHONE NUMBER API
    
    private func phoneNumberApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.phoneNumber(phone: phone_Number, verify: "0") { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController")as! OtpViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    isSocial = true
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    //MARK:- EMAIL NUMBER API
    
    private func registerEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.registerUserWithEmail(Email: tfEmail.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    if let res = resp!.value(forKey: "msg")as? String {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PasswordViewController")as! PasswordViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    
                }else if resp?.value(forKey: "code") as! NSNumber == 201{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigningUpViewController")as! SigningUpViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else {
                    
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    //MARK:- GOOGLE API
    
    private func checkRegisterWithSocialApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.registerWithSocial(social: social_media, social_id: id, auth_token: idToken) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                    
                    
                }else {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigningUpViewController")as! SigningUpViewController
                    isEmail = true
                    isSocial = false
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
        
        
    }
    
    //MARK:- GOOGLE
    
    private func googleDelegate(){
        
        GIDSignIn.sharedInstance().delegate   = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
        
    }
    
    func sign(_ signIn: GIDSignIn! , present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                     withError error: Error!) {
        
        if (error == nil) {
            
            
            id = user.userID                  // For client-side use only!
            idToken = user.authentication.idToken // Safe to send to the server
            first_Name = user.profile.givenName
            last_Name = user.profile.familyName
            username = user.profile.givenName + " " + user.profile.familyName
            email = user.profile.email
            if user.profile.hasImage{
                let image = user.profile.imageURL(withDimension: 200).absoluteString
                print("image",image)
                
            }
            social_media = "google"
            self.checkRegisterWithSocialApi()
            
        } else {
            print("\(error)")
        }
        
    }
    
    
    
    //MARK:- FACEBOOK
    
    private func loginFacebook(){
        let loginManager = LoginManager()
        loginManager.logIn(
            permissions: [.publicProfile, .email],
            viewController: self
        ) { result in
            
            print(result)
            
            switch result {
            case .cancelled:
                
                break
                
            case .failed(let error):
                
                break
                
            case .success(let grantedPermissions, _, let token):
                
                idToken = token!.tokenString
                
                self.getUserFBInformation(token: token!.tokenString)
            }
        }
    }
    func getUserFBInformation(token: String){
        let req = GraphRequest(graphPath: "/me", parameters: ["fields":"email,first_name,last_name,location{location}"], tokenString: token, version: nil, httpMethod: .get)
        req.start { [self] (connection, result, err) in
            
            if(err == nil) {
                print("result \(result)")
                let data = result as? [String:Any]
                username = "\((data!["first_name"])!) \((data!["last_name"])!)"
                first_Name = "\((data!["first_name"])!)"
                last_Name = "\((data!["last_name"])!)"
                email = "\((data!["email"])!)"
                id = "\((data!["id"])!)"
                social_media = "facebook"
                
                self.checkRegisterWithSocialApi()
                
            } else {
                print("error \(err)")
            }
        }
    }
    
    //MARK:- APPLE
    
    private func appleDelegate(){
        if #available(iOS 13.0, *) {
            let appleSignInRequest = ASAuthorizationAppleIDProvider().createRequest()
            appleSignInRequest.requestedScopes = [.fullName, .email]
            
            
            let controller = ASAuthorizationController(authorizationRequests: [appleSignInRequest])
            controller.delegate = self
            controller.presentationContextProvider = self as! ASAuthorizationControllerPresentationContextProviding
            
            controller.performRequests()
        }
    }
    
}

extension LoginViewController: ASAuthorizationControllerDelegate {
    
    func setupAppleIDCredentialObserver() {
        let authorizationAppleIDProvider = ASAuthorizationAppleIDProvider()
        authorizationAppleIDProvider.getCredentialState(forUserID: "currentUserIdentifier") { (credentialState: ASAuthorizationAppleIDProvider.CredentialState, error: Error?) in
            if let error = error {
                print(error)
                // Something went wrong check error state
                return
            }
            switch (credentialState) {
            case .authorized:
                //User is authorized to continue using your app
                break
            case .revoked:
                //User has revoked access to your app
                break
            case .notFound:
                //User is not found, meaning that the user never signed in through Apple ID
                break
            default: break
            }
        }
    }
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential else { return }
        
        print("User ID: \(appleIDCredential.user)")
        
        switch authorization.credential {
        
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            print(appleIDCredential)
            print(appleIDCredential.user)
            
        case let passwordCredential as ASPasswordCredential:
            print(passwordCredential)
        default: break
        }
        
        if let userEmail = appleIDCredential.email {
            print("Email: \(userEmail)")
            let keychainEmail = Keychain(service: "com.apple\(appleIDCredential.user)email")
            keychainEmail["email"] = userEmail
        }
        
        if let userGivenName = appleIDCredential.fullName?.givenName,
           let userFamilyName = appleIDCredential.fullName?.familyName {
            // dict.setValue(userGivenName, forKey: "username")
            let keychainfirstname = Keychain(service: "com.apple\(appleIDCredential.user)firstname")
            keychainfirstname["firstname"] = (appleIDCredential.fullName?.givenName)!
            
            let keychainlastname = Keychain(service: "com.apple\(appleIDCredential.user)lastname")
            keychainlastname["Lastname"] = (appleIDCredential.fullName?.familyName)!
            
        }
        if let authorizationCode = appleIDCredential.authorizationCode,
           let identifyToken = appleIDCredential.identityToken {
            print("Authorization Code: \(authorizationCode)")
            print("Identity Token: \(identifyToken)")
            
            
            let strAuthToken = String(decoding: authorizationCode, as: UTF8.self)
            print("StrAuthorization Code :\(strAuthToken)")
            let stridentifyToken = String(decoding: identifyToken, as: UTF8.self)
            print("StridentifyToken Code :\(stridentifyToken)")
            
            let keychain = Keychain(service: "com.apple\(appleIDCredential.user)")
            let keychainEmail = Keychain(service: "com.apple\(appleIDCredential.user)email")
            let keychainfirstname = Keychain(service: "com.apple\(appleIDCredential.user)firstname")
            let keychainlastname = Keychain(service: "com.apple\(appleIDCredential.user)lastname")
            
            print(keychainEmail["email"] ?? "")
            
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Authorization returned an error: \(error.localizedDescription)")
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        
        return view.window!
    }
}
