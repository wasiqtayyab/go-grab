//
//  PhoneNumberViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 05/08/2021.
//

import UIKit

class PhoneNumberViewController: UIViewController ,UITextFieldDelegate{
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var lblPhoneCode: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var countryView: UIView!
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfPhoneNumber.delegate = self
    
    }
    
    //MARK:- VIEW DID APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.lblPhoneCode.text = country_code1
    }

    //MARK:- BUTTON ACTION

    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        if lblPhoneCode.text == "code"{
            AppUtility?.showToast(string: "Select your country code", view: self.view)
            return
        }
        
        if !AppUtility!.isValidPhoneNumber(strPhone: tfPhoneNumber.text!){
            AppUtility?.showToast(string: "Enter a valid phone number", view: self.view)
            return
        }
        
        let trimmedString = AppUtility?.validate(tfPhoneNumber.text!)
        
        let index = trimmedString!.index(trimmedString!.startIndex, offsetBy: 0)
        String(trimmedString![index])
        print( String(trimmedString![index]) )
        if String(trimmedString![index]) == "+"{
            phone_Number = trimmedString!
        }else{
            phone_Number = "+92\(trimmedString)"
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "OtpViewController")as! OtpViewController
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    @IBAction func countryPressedButton(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CountryViewController")as! CountryViewController
        vc.modalPresentationStyle = .fullScreen
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
  
    
    //MARK:- TEXTFIELD DELEGATES

    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        
        if self.tfPhoneNumber.text != ""{
            
            if !AppUtility!.isValidPhoneNumber(strPhone: self.tfPhoneNumber.text!){
                btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            if lblPhoneCode.text == "code"{
                AppUtility?.showToast(string: "Select your country code", view: self.view)
                btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            
            btnNext.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
        }else {
            btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
        }
        
    }
    
}
