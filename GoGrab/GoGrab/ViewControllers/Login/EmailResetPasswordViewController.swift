//
//  EmailResetPasswordViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 06/08/2021.
//

import UIKit
import CocoaTextField

class EmailResetPasswordViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET

    @IBOutlet weak var btnResetLink: UIButton!
    @IBOutlet weak var emailBorderView: UIView!
    @IBOutlet weak var tfEmail: CocoaTextField!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureEmailTextField()
       
        btnResetLink.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
    }
    
    
    //MARK:- CONFIGURE
    
    private func configureEmailTextField(){
        self.tfEmail.text = email
        self.tfEmail.isUserInteractionEnabled = false
        tfEmail.delegate = self
        self.tfEmail.placeholder = "Email"
        self.tfEmail.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.borderWidth = 1.5
        self.tfEmail.defaultBackgroundColor = .clear
        self.tfEmail.focusedBackgroundColor = .clear
        self.tfEmail.cornerRadius = 7
        self.tfEmail.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
    }
    
    //MARK:- ACTION
    

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendResetLinkButtonPressed(_ sender: UIButton) {
        
        if !AppUtility!.isEmail(self.tfEmail.text!){
            AppUtility?.showToast(string: "Please enter your valid Email", view: self.view)
            return
        }
        
        email = self.tfEmail.text!
        forgetPasswordApi()
    }
    
    
    //MARK:- TEXTFIELD ACTION
    
    @IBAction func textfieldDidBegin(_ sender: UITextField) {
    
        emailBorderView.layer.borderWidth = 3
        emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        emailBorderView.layer.cornerRadius = 7
       
    }
    
    @IBAction func textfieldDidEnd(_ sender: UITextField) {
        
        emailBorderView.layer.borderWidth = 0
        emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        emailBorderView.layer.cornerRadius = 7
    
    }
    
    
    //MARK:- Forget Password Api
    
    private func forgetPasswordApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.forgotPassword(email: tfEmail.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmailOtpViewController")as! EmailOtpViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                   
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
 
}
