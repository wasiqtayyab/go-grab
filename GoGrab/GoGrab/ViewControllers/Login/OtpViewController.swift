//
//  OtpViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 08/07/2021.
//

import UIKit
import DPOTPView

class OtpViewController: UIViewController {
    
    //MARK:- Outlet
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var otpView: DPOTPView!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    
    
    //MARK:- View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.phoneNumberLabel.text = "We sent a code to \(phone_Number).Enter the code in that message."
        
        otpView.dpOTPViewDelegate = self
      
    }
    
    //MARK:- Button Action
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func tryAgainButtonPressed(_ sender: UIButton) {
        if isEmail == true {
            otpView.text = ""
            otpView.becomeFirstResponder()
            phoneNumberApi()
            
        }else {
            otpView.text = ""
            otpView.becomeFirstResponder()
            
        }
        
    }
    
    @IBAction func callMeInsteadButtonPressed(_ sender: UIButton) {
        
        
    }
    
    //MARK:- PHONE NUMBER API
    
    private func phoneNumberApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.phoneNumber(phone: phone_Number, verify: "0") { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    //MARK:- VERIFY CODE API
    
    private func verifyPhoneNumber(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyPhoneNumber(phone: phone_Number, verify: "1", code: otpView.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigningUpViewController")as! SigningUpViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                }else if resp?.value(forKey: "code") as! NSNumber == 202{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                    
                }else {
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
    
    //MARK:- REGISTER WITH SOCIAL MEDIA
    
    private func registerWithSocialApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.registerUserWithSocial(first_name: first_Name, username: username, last_name: last_Name, Email: email, DOB: DOB, Password: id, social: social_media, social_id: id, auth_token: idToken, role: "customer", country_id: country_id) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                    
                    
                }else {
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}


//MARK:- Textfield Delegate

extension OtpViewController: DPOTPViewDelegate {
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        
        if isEmail == true {
            if position == 3 && text.count == 4 {
                
                if isSocial == true {
                    otpView.dismissOnLastEntry = true
                    verifyPhoneNumber()
                    
                }else {
                    otpView.dismissOnLastEntry = true
                    registerWithSocialApi()
                    
                }
                
                
            }
        }else {
            if position == 3 && text.count == 4 {
                otpView.dismissOnLastEntry = true
                
            }
            
        }
        
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
        
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
        
    }
}
