//
//  SigningUpViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 07/07/2021.
//

import UIKit
import CocoaTextField
import ActiveLabel

class SigningUpViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfFirstName: CocoaTextField!
    @IBOutlet weak var tfSecondName: CocoaTextField!
    @IBOutlet weak var tfEmail: CocoaTextField!
    @IBOutlet weak var tfBirthday: CocoaTextField!
    
    @IBOutlet weak var tfPassword: CocoaTextField!
    
    @IBOutlet weak var firstNameBorder: UIView!
    @IBOutlet weak var secondNameBorder: UIView!
    @IBOutlet weak var emailNameBorder: UIView!
    @IBOutlet weak var lblTerm: ActiveLabel!
    @IBOutlet weak var birthdayNameBorder: UIView!
    @IBOutlet weak var checkedView: UIView!
    @IBOutlet weak var btnTick: UIButton!
    
    @IBOutlet weak var passwordBorderView: UIView!
    
    @IBOutlet weak var btnContinue: UIButton!
    
    @IBOutlet weak var btnShow: UIButton!
    
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var passwordViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    //MARK:- VARIABLE DECLERATION

    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    var date_Birth = ""
    var isSecurefield =  false
    var isTpped = false
    var SelectedDOB = ""
    var datePicker = UIDatePicker()
    var date = Date()
    var age : Int = 0
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenDetection()
        self.policyButton()
       
        self.showDatePickerTo()
        btnTick.isHidden = true
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    
    //MARK:- NAVIGATION
    
    private func screenDetection(){
        
        if isEmail == true {
            
            if isSocial == true {
                
                configureFirstNameTextField()
                configureLastNameTextField()
                configureBirthdayTextField()
                configureEmailTextField()
                configurePasswordTextField()
                configureView()
                passwordView.isHidden = true
                passwordViewConstraint.constant = 0
                
            }else {
            
                configureFirstNameTextField()
                configureLastNameTextField()
                configureBirthdayTextField()
                configureEmailTextField()
                configurePasswordTextField()
                configureView()
                passwordView.isHidden = true
                passwordViewConstraint.constant = 0
                tfFirstName.text = first_Name
                tfSecondName.text = last_Name
                tfEmail.text = email
                tfEmail.isUserInteractionEnabled = false
                
            }
            
        }else {
            
            passwordView.isHidden = false
            passwordViewConstraint.constant = 102
            configureFirstNameTextField()
            configureLastNameTextField()
            configureBirthdayTextField()
            configureEmailTextField()
            configurePasswordTextField()
            configureView()
            tfEmail.isUserInteractionEnabled = false
            tfEmail.text! = email
            btnBack.isHidden = false
            
            
        }
        
    }
    
    
    //MARK:- POLICY
    
    
    private func policyButton(){
        
        let customType = ActiveType.custom(pattern: "Term of Service,Payments Term of Service,Privacy Policy,")
        let customType1 = ActiveType.custom(pattern: "Nondiscrimination Policy.")
        lblTerm.enabledTypes = [.mention, .hashtag, .url, customType,customType1]
        lblTerm.text = "By Selecting Agree and continue below, I agree to GoGrab's Term of Service,Payments Term of Service,Privacy Policy, and Nondiscrimination Policy."
        lblTerm.textColor = UIColor.black
        lblTerm.customColor[customType1] = #colorLiteral(red: 0.1529411765, green: 0.337254902, blue: 0.6039215686, alpha: 1)
        lblTerm.customSelectedColor[customType1] = #colorLiteral(red: 0.1529411765, green: 0.337254902, blue: 0.6039215686, alpha: 1)
        lblTerm.customColor[customType] = #colorLiteral(red: 0.1529411765, green: 0.337254902, blue: 0.6039215686, alpha: 1)
        lblTerm.customSelectedColor[customType] = #colorLiteral(red: 0.1529411765, green: 0.337254902, blue: 0.6039215686, alpha: 1)
        lblTerm.handleCustomTap(for: customType) { element in
            print("Custom type tapped: \(element)")
        }
        
        lblTerm.handleCustomTap(for: customType1) { element in
            print("Custom type tapped: \(element)")
        }
        
    }
    
    
    //MARK:- CONFIGURE
    
    
    private func configureFirstNameTextField(){
        tfFirstName.delegate = self
        self.tfFirstName.placeholder = "First name"
        self.tfFirstName.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfFirstName.borderWidth = 1.5
        self.tfFirstName.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        self.tfFirstName.defaultBackgroundColor = .clear
        self.tfFirstName.focusedBackgroundColor = .clear
        self.tfFirstName.cornerRadius = 7
        self.tfFirstName.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfFirstName.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        
    }
    
    
    private func configureLastNameTextField(){
        tfSecondName.delegate = self
        self.tfSecondName.placeholder = "Last name"
        self.tfSecondName.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfSecondName.borderWidth = 1.5
        self.tfSecondName.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        self.tfSecondName.defaultBackgroundColor = .clear
        self.tfSecondName.focusedBackgroundColor = .clear
        self.tfSecondName.cornerRadius = 7
        self.tfSecondName.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfSecondName.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }
    
    private func configureEmailTextField(){
        tfEmail.delegate = self
        self.tfEmail.placeholder = "Email"
        self.tfEmail.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.borderWidth = 1.5
        self.tfEmail.defaultBackgroundColor = .clear
        self.tfEmail.focusedBackgroundColor = .clear
        self.tfEmail.cornerRadius = 7
        self.tfEmail.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
    }
    
    
    private func configureBirthdayTextField(){
        tfBirthday.delegate = self
        self.tfBirthday.placeholder = "Birthday (mm/dd/yyyy)"
        self.tfBirthday.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfBirthday.borderWidth = 1.5
        self.tfBirthday.defaultBackgroundColor = .clear
        self.tfBirthday.focusedBackgroundColor = .clear
        self.tfBirthday.cornerRadius = 7
        self.tfBirthday.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfBirthday.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        
    }
    
    
    private func configurePasswordTextField(){
        tfPassword.delegate = self
        self.tfPassword.placeholder = "Password"
        self.tfPassword.clearButtonMode = .never
        self.tfPassword.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPassword.borderWidth = 1.5
        self.tfPassword.defaultBackgroundColor = .clear
        self.tfPassword.focusedBackgroundColor = .clear
        self.tfPassword.cornerRadius = 7
        self.tfPassword.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPassword.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        
    }
    
    
    private func configureView(){
        checkedView.layer.borderWidth = 1.5
        checkedView.layer.borderColor = #colorLiteral(red: 0.8941176471, green: 0.8941176471, blue: 0.8941176471, alpha: 1)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        checkedView.addGestureRecognizer(tap)
        
    }
    
    
    //MARK:- TEXTFIELD ACTION
    
    @IBAction func textfieldDidBegin(_ sender: UITextField) {
        
        if sender == tfFirstName {
            firstNameBorder.layer.borderWidth = 3
            firstNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            firstNameBorder.layer.cornerRadius = 7
            firstNameBorder.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            
        }else if sender == tfSecondName {
            
            secondNameBorder.layer.borderWidth = 3
            secondNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            secondNameBorder.layer.cornerRadius = 7
            secondNameBorder.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            
        }else if sender == tfEmail {
            
            emailNameBorder.layer.borderWidth = 3
            emailNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            emailNameBorder.layer.cornerRadius = 7
            
        }else if sender == tfBirthday {
            
            birthdayNameBorder.layer.borderWidth = 3
            birthdayNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            birthdayNameBorder.layer.cornerRadius = 7
            
            
        }else {
            passwordBorderView.layer.borderWidth = 3
            passwordBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            passwordBorderView.layer.cornerRadius = 7
            
        }
        
    }
    
    @IBAction func textfieldDidEnd(_ sender: UITextField) {
        
        if sender == tfFirstName {
            firstNameBorder.layer.borderWidth = 0
            firstNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            firstNameBorder.layer.cornerRadius = 7
            firstNameBorder.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        }else if sender == tfSecondName {
            
            secondNameBorder.layer.borderWidth = 0
            secondNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            secondNameBorder.layer.cornerRadius = 7
            secondNameBorder.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            
        }else if sender == tfEmail {
            emailNameBorder.layer.borderWidth = 0
            emailNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            emailNameBorder.layer.cornerRadius = 7
            
        }else if sender == tfBirthday {
            
            birthdayNameBorder.layer.borderWidth = 0
            birthdayNameBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            birthdayNameBorder.layer.cornerRadius = 7
            
        }else {
            passwordBorderView.layer.borderWidth = 0
            passwordBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            passwordBorderView.layer.cornerRadius = 7
            
        }
    }
    
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func showButtonPressed(_ sender: UIButton) {
        
        if self.isSecurefield == false{
            
            let hide = NSAttributedString(string: "Hide",
                                          attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
            
            self.tfPassword.isSecureTextEntry =  false
            self.isSecurefield =  true
            self.btnShow.setAttributedTitle(hide, for: .normal)
            
        }else{
            let show = NSAttributedString(string: "Show",
                                          attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
            self.tfPassword.isSecureTextEntry =  true
            self.isSecurefield =  false
            self.btnShow.setAttributedTitle(show, for: .normal)
            
        }
        
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if isTpped == true {
            btnTick.isHidden = true
            isTpped = false
        }else {
            
            btnTick.isHidden = false
            isTpped = true
        }
        
        
    }
    
    @objc func cancelDatePickerTo(){
        self.view.endEditing(true)
    }
    
    @objc func donedatePickerTo(){
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "MM/dd/yyyy"
        self.tfBirthday.text! = "\(formatter1.string(from: datePicker.date))"
        print("Date Of Registration : \(self.tfBirthday.text!)")
        date_Birth = (AppUtility?.convertDateFormater1(tfBirthday.text!))!
        print("date_Birth",date_Birth)
        self.view.endEditing(true)
        
    }
    
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        
        if isEmail == true {
            if AppUtility!.isEmpty(self.tfFirstName.text!){
                AppUtility?.showToast(string: "Please enter your First Name", view: self.view)
                return
            }
            
            if AppUtility!.isEmpty(self.tfSecondName.text!){
                AppUtility?.showToast(string: "Please enter your Second Name", view: self.view)
                return
            }
            
            if AppUtility!.isEmpty(self.tfBirthday.text!){
                AppUtility?.showToast(string: "Please enter your Birthday", view: self.view)
                return
            }
            
            if !AppUtility!.isEmail(self.tfEmail.text!){
                AppUtility?.showToast(string: "Please enter your valid Email", view: self.view)
                return
            }
            
            let birthday_age = AppUtility!.calculateAge(dob: tfBirthday.text!, format: "MM/dd/yyyy")
            age = Int(birthday_age)!
            
            if age < 18 {
                AppUtility?.showToast(string: "your age is less than 18", view: self.view)
                return
            }
            
            first_Name = self.tfFirstName.text!
            last_Name = self.tfSecondName.text!
            email = self.tfEmail.text!
            DOB = (AppUtility?.convertDateFormater(date_Birth))!
            
           
            
            if isSocial == true {
                self.registerUserApi()
                
            }else {
               
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhoneNumberViewController")as! PhoneNumberViewController
                self.navigationController?.pushViewController(vc, animated: true)
               
            }
            
            
            
        }else {
            if AppUtility!.isEmpty(self.tfFirstName.text!){
                AppUtility?.showToast(string: "Please enter your First Name", view: self.view)
                return
            }
            
            if AppUtility!.isEmpty(self.tfSecondName.text!){
                AppUtility?.showToast(string: "Please enter your Second Name", view: self.view)
                return
            }
            
            if AppUtility!.isEmpty(self.tfBirthday.text!){
                AppUtility?.showToast(string: "Please enter your Birthday", view: self.view)
                return
            }
            
            if tfPassword.text!.count <= 7{
                AppUtility?.showToast(string: "Please enter your valid password", view: self.view)
                return
            }
            
            if !AppUtility!.isEmail(self.tfEmail.text!){
                AppUtility?.showToast(string: "Please enter your valid Email", view: self.view)
                return
            }
            
            let birthday_age = AppUtility!.calculateAge(dob: tfBirthday.text!, format: "MM/dd/yyyy")
            age = Int(birthday_age)!
            if age < 18 {
                AppUtility?.showToast(string: "your age is less than 18", view: self.view)
                return
            }
            
            first_Name = self.tfFirstName.text!
            last_Name = self.tfSecondName.text!
            email = self.tfEmail.text!
           
            password = self.tfPassword.text!

            DOB = (AppUtility?.convertDateFormater(date_Birth))!
            
            registerUserApi()
          
        }
        
        
    }

    //MARK:- Date Picker
    private func showDatePickerTo(){
        
        datePicker.datePickerMode = .date
        let calendar = Calendar(identifier: .gregorian)
        
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        
        components.year = -19
        components.month = 12
        let maxDate = calendar.date(byAdding: components, to: currentDate)!
        
        components.year = -180
        let minDate = calendar.date(byAdding: components, to: currentDate)!
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerTo));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        self.tfBirthday.inputAccessoryView = toolbar
        self.tfBirthday.inputView = datePicker
        
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if isEmail == true {
            
            if self.tfFirstName.text != "" && self.tfSecondName.text != "" && self.tfBirthday.text != "" && self.tfEmail.text != "" {
                
                
                if !AppUtility!.isEmail(self.tfEmail.text!){
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                let birthday_age = AppUtility!.calculateAge(dob: tfBirthday.text!, format: "MM/dd/yyyy")
                age = Int(birthday_age)!
                if age < 18 {
                    AppUtility?.showToast(string: "your age is less than 18", view: self.view)
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                
                btnContinue.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
               
            }else {
                btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                
            }
            
        }
        
        else  {
            
            if self.tfFirstName.text != "" && self.tfSecondName.text != "" && self.tfBirthday.text != "" && self.tfEmail.text != "" && self.tfPassword.text! != "" {
                
                if !AppUtility!.isEmail(self.tfEmail.text!){
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                let birthday_age = AppUtility!.calculateAge(dob: tfBirthday.text!, format: "MM/dd/yyyy")
                age = Int(birthday_age)!
                if age < 18 {
                    AppUtility?.showToast(string: "your age is less than 18", view: self.view)
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                if tfPassword.text!.count <= 7 {
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                
                btnContinue.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
            }else {
                btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                
            }
            
        }
        
    }
    
    //MARK:- REGISTER USER API
    
    private func registerUserApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.registerUser(first_name: tfFirstName.text!, username: tfFirstName.text! + tfSecondName.text!, last_name: tfSecondName.text!, Email: tfEmail.text!, DOB: date_Birth, Password: self.tfPassword.text!, phone: phone_Number) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
      
}
