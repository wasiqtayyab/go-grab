//
//  PasswordViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 06/08/2021.
//

import UIKit
import CocoaTextField

class PasswordViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfPassword: CocoaTextField!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var passwordViewBorder: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK:- Vars
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    var isSecurefield =  false
    var myUser: [User]? {didSet {}}
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinue.isUserInteractionEnabled = true
        configurePasswordTextField()
      
        
    }
    
    //MARK:- CONFIGURE
    
    private func configurePasswordTextField(){
        
        tfPassword.delegate = self
        self.tfPassword.placeholder = "Password"
        self.tfPassword.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPassword.borderWidth = 1.5
        self.tfPassword.defaultBackgroundColor = .clear
        self.tfPassword.focusedBackgroundColor = .clear
        self.tfPassword.cornerRadius = 7
        self.tfPassword.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPassword.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.tfPassword.clearButtonMode = .never
        
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    @IBAction func textfieldDidBegin(_ sender: UITextField) {
        
        passwordViewBorder.layer.borderWidth = 3
        passwordViewBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        passwordViewBorder.layer.cornerRadius = 7
        
    }
    
    @IBAction func textfieldDidEnd(_ sender: UITextField) {
        
        passwordViewBorder.layer.borderWidth = 0
        passwordViewBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        passwordViewBorder.layer.cornerRadius = 7
        
        
    }
    
    //MARK:- ACTION
    
    
    @IBAction func showButtonPressed(_ sender: UIButton) {
        
        if self.isSecurefield == false{
            
            let show = NSAttributedString(string: "Hide",
                                          attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
            self.tfPassword.isSecureTextEntry =  false
            self.btnShow.setAttributedTitle(show, for: .normal)
            self.isSecurefield =  true
            
            
        }else{
            let show = NSAttributedString(string: "Show",
                                          attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
            self.tfPassword.isSecureTextEntry =  true
            self.btnShow.setAttributedTitle(show, for: .normal)
            self.isSecurefield =  false
            
            
        }
        
    }
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        if tfPassword.text!.count <= 7 {
            btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            return
        }
        loginApi()

    }
    
    @IBAction func forgetPasswordButtonPressed(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmailResetPasswordViewController")as! EmailResetPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if self.tfPassword.text != ""{
            
            if AppUtility!.isEmpty(tfPassword.text!){
                AppUtility?.showToast(string: "Please enter a password", view: self.view)
                btnContinue.isUserInteractionEnabled = false
                btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            if tfPassword.text!.count <= 7 {
                btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            btnContinue.isUserInteractionEnabled = true
            btnContinue.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
        }else {
            btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            
        }
        
    }
    
    //MARK:- LOGIN API
    
    private func loginApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.login(Email: email, Password: tfPassword.text!) { [self] (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
        
    }
   
    
    
}
