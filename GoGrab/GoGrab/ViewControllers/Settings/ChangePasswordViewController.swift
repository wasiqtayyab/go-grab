//
//  ChangePasswordViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 13/08/2021.
//

import UIKit

class ChangePasswordViewController: UIViewController,UITextFieldDelegate {
    //MARK:- OUTLET
    
    @IBOutlet weak var tfOldPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnOldPasswordShow: UIButton!
    @IBOutlet weak var btnNewPasswordShow: UIButton!
    @IBOutlet weak var btnConfirmPasswordShow: UIButton!
    
    @IBOutlet weak var btnUpdatePassword: UIButton!
    
    //MARK:-Vars
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    var isSecure1 = true
    var isSecure2 = true
    var isSecure3 = true
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfNewPassword.delegate = self
        tfConfirmPassword.delegate = self
        tfOldPassword.delegate = self

    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func showButtonPressed(_ sender: UIButton) {
        
        if sender == btnOldPasswordShow{
            
            if isSecure1 == true {
                tfOldPassword.isSecureTextEntry = false
                btnOldPasswordShow.setTitle("Hide", for: .normal)
                isSecure1 = false
                
            }else {
                
                tfOldPassword.isSecureTextEntry = true
                btnOldPasswordShow.setTitle("Show", for: .normal)
                isSecure1 = true
                
                
            }
            
            
            
        }else if sender == btnNewPasswordShow {
            
           
            if isSecure2 == true {
                tfNewPassword.isSecureTextEntry = false
                btnNewPasswordShow.setTitle("Hide", for: .normal)
                isSecure2 = false
                
            }else {
                
                tfNewPassword.isSecureTextEntry = true
                btnNewPasswordShow.setTitle("Show", for: .normal)
                isSecure2 = true
                
                
            }
            
            
            
        }else {
            
           
            if isSecure3 == true {
                tfConfirmPassword.isSecureTextEntry = false
                btnConfirmPasswordShow.setTitle("Hide", for: .normal)
                isSecure3 = false
                
            }else {
                
                tfConfirmPassword.isSecureTextEntry = true
                btnConfirmPasswordShow.setTitle("Show", for: .normal)
                isSecure3 = true
                
                
            }
            
        }
        
    }
    
    @IBAction func updatePasswordButtonPressed(_ sender: UIButton) {
        
        if tfOldPassword.text == tfNewPassword.text {
            AppUtility?.showToast(string: "Your old password and new password should be different", view: self.view)
            return
        }
        
        if self.tfNewPassword.text == self.tfConfirmPassword.text {
            changePassword()
          
        }else {
           
            AppUtility?.showToast(string: "Your password does not match", view: self.view)
            
        }
        
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- CHANGE PASSWORD API
    
    private func changePassword(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changePassword(user_id: id, old_password: tfOldPassword.text!, new_password: tfNewPassword.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: EditProfileViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
      
                }else{
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
    
   
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if tfNewPassword.text != "" && tfConfirmPassword.text != "" && tfOldPassword.text != "" {
            
            if tfOldPassword.text!.count <= 8 {
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            if tfNewPassword.text!.count <= 8 {
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            if tfConfirmPassword.text!.count <= 8 {
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            
            if tfNewPassword.text == tfConfirmPassword.text {
                
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
  
            }else {

                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
        
            }
            
        }else {
            btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            
        }
    }
    
}
