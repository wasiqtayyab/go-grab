//
//  SettingsTableViewCell.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 08/07/2021.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAccount: UILabel!

    @IBOutlet weak var imgAccount: UIImageView!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
