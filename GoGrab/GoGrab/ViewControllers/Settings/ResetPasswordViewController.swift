//
//  ResetPasswordViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 02/06/2021.
//

import UIKit
import CocoaTextField

class ResetPasswordViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfNewPassword: CocoaTextField!
    @IBOutlet weak var newPasswordBorderView: UIView!
    @IBOutlet weak var btnShowNewPassword: UIButton!
    
    @IBOutlet weak var tfConfirmPassword: CocoaTextField!
    @IBOutlet weak var confirmPasswordBorderView: UIView!
    @IBOutlet weak var btnShowConfirmPassword: UIButton!
    
    @IBOutlet weak var btnUpdatePassword: UIButton!
    
    
    //MARK:- Vars
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    var isSecurefield =  false
    var isSecurefield1 =  false
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNewPasswordTextField()
        configureConfirmPasswordTextField()
        
    }
    
    
    //MARK:- CONFIGURE
    
    private func configureNewPasswordTextField(){
        
        tfNewPassword.delegate = self
        self.tfNewPassword.placeholder = "New Password"
        self.tfNewPassword.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfNewPassword.borderWidth = 1.5
        self.tfNewPassword.defaultBackgroundColor = .clear
        self.tfNewPassword.focusedBackgroundColor = .clear
        self.tfNewPassword.cornerRadius = 7
        self.tfNewPassword.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfNewPassword.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.tfNewPassword.clearButtonMode = .never
        
    }
    
    private func configureConfirmPasswordTextField(){
        
        tfConfirmPassword.delegate = self
        self.tfConfirmPassword.placeholder = "Confirm Password"
        self.tfConfirmPassword.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfConfirmPassword.borderWidth = 1.5
        self.tfConfirmPassword.defaultBackgroundColor = .clear
        self.tfConfirmPassword.focusedBackgroundColor = .clear
        self.tfConfirmPassword.cornerRadius = 7
        self.tfConfirmPassword.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfConfirmPassword.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.tfConfirmPassword.clearButtonMode = .never
        
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if self.tfNewPassword.text != "" && self.tfConfirmPassword.text != ""{
            
            if tfNewPassword.text!.count < 8 {
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            if tfConfirmPassword.text!.count < 8 {
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            if tfNewPassword.text == tfConfirmPassword.text {
                
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
                
            }else {
                
                AppUtility?.showToast(string: "Your password does not match", view: self.view)
                btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            }
            
        }else {
            
            btnUpdatePassword.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
        }
        
    }
   
    //MARK:- TEXTFIELD ACTION
    
    @IBAction func textfieldDidBegin(_ sender: UITextField) {
        
        if sender == tfNewPassword{
            newPasswordBorderView.layer.borderWidth = 3
            newPasswordBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            newPasswordBorderView.layer.cornerRadius = 7
        }else {
            confirmPasswordBorderView.layer.borderWidth = 3
            confirmPasswordBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            confirmPasswordBorderView.layer.cornerRadius = 7
            
        }
        
    }
    
    @IBAction func textfieldDidEnd(_ sender: UITextField) {
        
        if sender == tfNewPassword{
            newPasswordBorderView.layer.borderWidth = 0
            newPasswordBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            newPasswordBorderView.layer.cornerRadius = 7
            
        }else {
            confirmPasswordBorderView.layer.borderWidth = 0
            confirmPasswordBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            confirmPasswordBorderView.layer.cornerRadius = 7
            
        }
        
        
    }
    
    //MARK:- ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func updatePasswordButtonPressed(_ sender: UIButton) {
        
        if tfNewPassword.text!.count < 8 {
            AppUtility?.showToast(string: "Your password less than 8 digit", view: self.view)
            return
        }
        if tfConfirmPassword.text!.count < 8 {
            AppUtility?.showToast(string: "Your password less than 8 digit", view: self.view)
            return
        }
        
        if self.tfNewPassword.text == self.tfConfirmPassword.text {
            changeEmailPasswordApi()
          
        }else {
           
            AppUtility?.showToast(string: "Your password does not match", view: self.view)
            
        }
        
       
        
    }
    
    
    @IBAction func showButtonPressed(_ sender: UIButton){
        
        
        if sender == btnShowNewPassword{
            
            if self.isSecurefield == false{
                
                let show = NSAttributedString(string: "Hide",
                                              attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
                self.tfNewPassword.isSecureTextEntry =  false
                self.btnShowNewPassword.setAttributedTitle(show, for: .normal)
                self.isSecurefield =  true
                
                
            }else{
                let show = NSAttributedString(string: "Show",
                                              attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
                self.tfNewPassword.isSecureTextEntry =  true
                self.btnShowNewPassword.setAttributedTitle(show, for: .normal)
                self.isSecurefield =  false
                
                
            }
            
            
        }else {
            
            if self.isSecurefield1 == false{
                
                let show = NSAttributedString(string: "Hide",
                                              attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
                self.tfConfirmPassword.isSecureTextEntry =  false
                self.btnShowConfirmPassword.setAttributedTitle(show, for: .normal)
                self.isSecurefield1 =  true
                
                
            }else{
                let show = NSAttributedString(string: "Show",
                                              attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
                self.tfConfirmPassword.isSecureTextEntry =  true
                self.btnShowConfirmPassword.setAttributedTitle(show, for: .normal)
                self.isSecurefield1 =  false
                
                
            }
            
        }
        
    }
    
    //MARK:- Change Email Password Api
    
    private func changeEmailPasswordApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changePasswordForgot(email: email, password: tfNewPassword.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isHidden = true
                    self.view.window?.rootViewController = nav
                        
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
}
