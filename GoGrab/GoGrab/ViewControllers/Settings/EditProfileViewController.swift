//
//  EditProfileViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 12/08/2021.
//

import UIKit

class EditProfileViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfGender: UITextField!
    @IBOutlet weak var tfBirth: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    
    
    //MARK:- Vars
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    let defaults = UserDefaults.standard
    var date_Birth = ""
    var datePicker = UIDatePicker()
    var isSelected = true
    var age : Int = 0
    var selectedGender: String?
    var myPickerView : UIPickerView!
    var pickerData = ["Unspecified","Male","Female"]
    var result = ""
    var myUser: [User]? {didSet {}}
    var myCountry: [UserCountry]? {didSet {}}
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        readDataFromArchive()
        DOB = (AppUtility?.convertDateFormater(date_Birth))!
        setupDelegate()
        tfGender.placeholder = "Select Gender"
    }
    
    
    //MARK:- READ DATA
    
    private func readDataFromArchive(){
        self.myUser = User.readUserFromArchive()
        self.myCountry = UserCountry.readUserCountryFromArchive()
        if (myUser != nil && myUser?.count != 0) {
            id = myUser?[0].Id ?? ""
            
        }else{
            id = ""
        }
        if (myUser?.count != 0){
            
            date_Birth = myUser?[0].dob ?? ""
            first_Name = myUser?[0].first_name ?? ""
            last_Name = myUser?[0].last_name ?? ""
            gender = myUser?[0].gender ?? ""
            email = myUser?[0].email ?? ""
            phone_Number = myUser?[0].phone ?? ""
            
        }
    }
    
    //MARK:- SETUP DELEGATE
    
    private func setupDelegate(){
    
        tfFirstName.text! = first_Name
        tfLastName.text! = last_Name
        tfBirth.text! = DOB
        tfEmail.text! = email
        tfPhoneNumber.text! = phone_Number
        tfGender.text! = gender
        self.showDatePickerTo()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            
        }
        tfEmail.isUserInteractionEnabled = false
        tfPhoneNumber.isUserInteractionEnabled = false
        tfGender.delegate = self
        self.createPickerView()
        
        
    }
    
    //MARK:- UIPICKER Gender
    
    func createPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        tfGender.inputView = pickerView
        tfBirth.delegate = self
    }
    
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        tfGender.inputAccessoryView = toolBar
    }
    
    
    //MARK:- UIPICKER DELEGATE
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedGender = pickerData[row]
        tfGender.text = selectedGender
    }
    
    //MARK:- BUTTON ACTION
    
    @objc func action() {
        view.endEditing(true)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        if AppUtility!.isEmpty(tfGender.text!){
            AppUtility?.showToast(string: "Select your gender", view: self.view)
            return
        }
        
        if AppUtility!.isEmpty(tfBirth.text!){
            AppUtility?.showToast(string: "Enter your birthday", view: self.view)
            return
        }
        let birthday_age = AppUtility!.calculateAge(dob: tfBirth.text!, format: "MM/dd/yyyy")
        age = Int(birthday_age)!
        if age < 18 {
            AppUtility?.showToast(string: "your age is less than 18", view: self.view)
            return
        }
        date_Birth = (AppUtility?.convertDateFormater1(tfBirth.text!))!
        
        
        editProfileApi()
        
    }
    
    
    @objc func cancelDatePickerTo(){
        self.view.endEditing(true)
    }
    
    @objc func donedatePickerTo(){
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "MM/dd/yyyy"
        self.tfBirth.text! = "\(formatter1.string(from: datePicker.date))"
        print("Date Of Registration : \(self.tfBirth.text!)")
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func emailButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "UpdateEmailViewController")as! UpdateEmailViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func phoneNumberButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddPhoneNumberViewController")as! AddPhoneNumberViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
     
    //MARK:- TEXTFIELD ACTION
    
    @IBAction func genderTextfieldPressed(_ sender: UITextField) {
        createPickerView()
    }
    
    //MARK:- Date Picker For Birthday
    private func showDatePickerTo(){
        
        datePicker.datePickerMode = .date
        
        let calendar = Calendar(identifier: .gregorian)
        
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        
        components.year = -19
        components.month = 12
        let maxDate = calendar.date(byAdding: components, to: currentDate)!
        
        components.year = -180
        let minDate = calendar.date(byAdding: components, to: currentDate)!
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
       
         let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerTo));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        self.tfBirth.inputAccessoryView = toolbar
        self.tfBirth.inputView = datePicker
        
    }
    
    //MARK:- EDIT PROFILE API WITH IMAGE
    
    
    private func editProfileApi(){
        self.loader.isHidden = false
        self.myUser = User.readUserFromArchive()
        image_URL = defaults.object(forKey: "imageUrl") as! String
        ApiHandler.sharedInstance.editProfile(user_id: id, first_name: tfFirstName.text!, last_name: tfLastName.text!, gender: tfGender.text!, dob: date_Birth, phone: tfPhoneNumber.text!, image: image_URL) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: SettingsViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}
