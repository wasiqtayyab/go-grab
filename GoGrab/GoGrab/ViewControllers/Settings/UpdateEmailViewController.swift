//
//  UpdateEmailViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 12/08/2021.
//

import UIKit
import CocoaTextField

class UpdateEmailViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnNewPassword: UIButton!
    @IBOutlet weak var tfEmail: CocoaTextField!
    @IBOutlet weak var emailBorderView: UIView!
    @IBOutlet weak var btnSave: UIButton!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureEmailTextField()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isEmail == true {
            btnNewPassword.isHidden = true
        }else {
            btnNewPassword.isHidden = false
            
        }
    }
    
    //MARK:- CONFIGURE
    
    private func configureEmailTextField(){
        tfEmail.delegate = self
        self.tfEmail.placeholder = "Email"
        self.tfEmail.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.borderWidth = 1.5
        self.tfEmail.defaultBackgroundColor = .clear
        self.tfEmail.focusedBackgroundColor = .clear
        self.tfEmail.cornerRadius = 7
        self.tfEmail.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfEmail.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        if  !AppUtility!.isEmail(self.tfEmail.text!){
            AppUtility?.showToast(string: "Please enter your valid Email", view: self.view)
            return
        }
        email = self.tfEmail.text!
        updateEmailApi()
        
    }
    
    @IBAction func needNewPasswordButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController")as! ChangePasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- TEXTFIELD ACTION
    
    @IBAction func textfieldDidBegin(_ sender: UITextField) {
        
        emailBorderView.layer.borderWidth = 3
        emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        emailBorderView.layer.cornerRadius = 7
        
    }
    
    @IBAction func textfieldDidEnd(_ sender: UITextField) {
        
        emailBorderView.layer.borderWidth = 0
        emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        emailBorderView.layer.cornerRadius = 7
        
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if self.tfEmail.text != "" {
            
            
            if !AppUtility!.isEmail(self.tfEmail.text!){
                btnSave.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            btnSave.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
        }else {
            btnSave.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            
        }
    }
    
    //MARK:- UPDATE EMAIL API
    
    private func updateEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changeEmailAddress(email: tfEmail.text!, user_id: id) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhoneNumberOtpViewController")as! PhoneNumberOtpViewController
                    isPhone = false
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
    
}


