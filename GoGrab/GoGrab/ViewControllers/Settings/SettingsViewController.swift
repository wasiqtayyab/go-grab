//
//  SettingsViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit
import SDWebImage

class SettingsViewController: UIViewController,UIImagePickerControllerDelegate{
    
    //MARK:- OUTLET
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIButton!
    @IBOutlet weak var getHelpView: UIView!
    
    
    //MARK:- Vars
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    let defaults = UserDefaults.standard
    var myUser: [User]? {didSet {}}
    var myCountry: [UserCountry]? {didSet {}}
    let imgPickerController = UIImagePickerController()
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        defaults.removeObject(forKey: "imageUrl")
        imageCircle()
        self.imgPickerController.delegate =  self
        getHelpRecognizer()
       
    }
    
    //MARK:- VIEW WILL APPEAR
    
    override func viewWillAppear(_ animated: Bool) {
        
       
        readDataFromArchive()
        self.nameLabel.text = username
        if image != "" {
            image_URL = (profileImage.currentImage!.jpegData(compressionQuality: 0.5)?.base64EncodedString())!
            
            let imgURL = BASE_URL + image
            print("imgURL:",imgURL)
            profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            profileImage.sd_setImage(with: URL(string: imgURL), for: .normal)
            
        }else {
            profileImage.setImage(UIImage(named:"2-3"), for: .normal)
            image_URL = (AppUtility?.convertImageToBase64(image: UIImage(named:"2-3")!))!
            defaults.setValue(image_URL, forKey: "imageUrl")
        }
    }
    
    //MARK:- READ DATA
    
    private func readDataFromArchive(){
        self.myUser = User.readUserFromArchive()
        self.myCountry = UserCountry.readUserCountryFromArchive()
        if (myUser?.count != 0){
    
            image = myUser?[0].image ?? ""
            username = (myUser?[0].first_name)! + (myUser?[0].last_name)!
           
        }
    }
    
    //MARK:- IMAGE CIRCLE
    
    private func imageCircle(){

        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.width / 2

    }

    //MARK:- TAP GESTURES
    
    private func getHelpRecognizer(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(getHelpPressed(_:)))
        getHelpView.addGestureRecognizer(tap)

    }
    
    //MARK:- LOG OUT API
    
    private func logOutApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.logout(user_id: id) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    self.myUser = User.readUserFromArchive()
                    self.myCountry = UserCountry.readUserCountryFromArchive()
                    self.myCountry?.remove(at: 0)
                    self.myUser?.remove(at: 0)
                    if User.saveUserToArchive(user: self.myUser!){
                        
                        let story = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    let nav = UINavigationController(rootViewController: vc)
                                    nav.navigationBar.isHidden = true
                                    self.view.window?.rootViewController = nav
                        
                        
                    }else if resp?.value(forKey: "code") as! NSNumber == 201 {
                        
                        AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                        
                    }
                    
                }else{
                    print(resp as Any)
                }
            }
            
        }
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func paymentButtonPressed(_ sender: UIButton) {
        print("done")
        self.tabBarController?.selectedIndex =  1
        
    }
    
    
    @IBAction func personalInformationButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController")as! EditProfileViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func notificationButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func imageButtonPressed(_ sender: UIButton) {
        let tab = UIAlertController(title:"Please Select", message: nil, preferredStyle:
                                        .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (alert:UIAlertAction) in
            self.imgPickerController.sourceType = .camera
            self.imgPickerController.cameraCaptureMode = .photo
            
            self.present(self.imgPickerController, animated: true, completion: nil)
            
        }
        let PhotoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (alert:UIAlertAction) in
            self.imgPickerController.sourceType = .photoLibrary
            self.present(self.imgPickerController, animated: true, completion: nil)
            
        }
        let cancel = UIAlertAction(title: "Cancel", style:.cancel, handler: nil)
        
        tab.addAction(camera)
        tab.addAction(PhotoLibrary)
        tab.addAction(cancel)
        self.present(tab, animated: true, completion: nil)
    }
    
    @objc func getHelpPressed(_ sender: UITapGestureRecognizer){
        let vc = storyboard?.instantiateViewController(withIdentifier: "ReportProblemViewController") as! ReportProblemViewController
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func logOutButtonPressed(_ sender: UIButton) {
        
        let alertController = UIAlertController(title:"Logout", message:"Are sure to want to logout" , preferredStyle: .alert)
        let OK  =  UIAlertAction(title: "OK", style: .default) { [self] (UIAlertAction) in
            
            self.logOutApi()
            defaults.removeObject(forKey: "imageUrl")
          
        }
        let Cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(OK)
        alertController.addAction(Cancel)
        Cancel.setValue(UIColor.red, forKey: "titleTextColor")
        self.present(alertController, animated: true, completion: nil)
       
        
    }
    
    //MARK: Image Picker Delegate
    
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("didFinsih picker call")
        
        if let pickedImage = info[.originalImage] as? UIImage {
            image_URL = (pickedImage.jpegData(compressionQuality: 0.5)?.base64EncodedString())!

            self.profileImage.contentMode = .scaleAspectFit
            profileImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.profileImage.setImage(pickedImage, for: .normal)
            editProfileApi()
        }
       
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- EDIT PROFILE API
    
    private func editProfileApi(){
        self.loader.isHidden = false
        self.myUser = User.readUserFromArchive()
        first_Name = (self.myUser?[0].first_name)!
        last_Name = (self.myUser?[0].last_name)!
        gender = (self.myUser?[0].gender)!
        DOB = (self.myUser?[0].dob)!
        phone_Number = (self.myUser?[0].phone)!
        
        ApiHandler.sharedInstance.editProfile(user_id: id, first_name: first_Name, last_name: last_Name, gender: gender, dob: DOB, phone: phone_Number, image: image_URL) { [self] (isSuccess, resp) in
            self.loader.isHidden = true
        
            if isSuccess {
               print("resp",resp)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    defaults.setValue(image_URL, forKey: "imageUrl")
                    self.dismiss(animated: true, completion: nil)
                   
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}


