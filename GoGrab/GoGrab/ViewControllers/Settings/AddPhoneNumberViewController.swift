//
//  AddPhoneNumberViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 16/08/2021.
//

import UIKit
import CocoaTextField

class AddPhoneNumberViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfCountryRegion: CocoaTextField!
    @IBOutlet weak var tfPhoneNumber: CocoaTextField!
    @IBOutlet weak var countryBorderView: UIView!
    @IBOutlet weak var phoneBorderView: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    
    
    //MARK:- Vars
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    //MARK:- VIEW WILL APPEAR
    
    override func viewWillAppear(_ animated: Bool) {
        configureCountryTextField()
        configurePhoneTextField()
        self.tfCountryRegion.text = country_code
    }
   
    //MARK:- Configure
    
    private func configureCountryTextField(){
        
        tfCountryRegion.delegate = self
        self.tfCountryRegion.placeholder = "Country/Region"
        self.tfCountryRegion.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfCountryRegion.borderWidth = 1.5
        self.tfCountryRegion.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        self.tfCountryRegion.defaultBackgroundColor = .clear
        self.tfCountryRegion.focusedBackgroundColor = .clear
        self.tfCountryRegion.cornerRadius = 7
        self.tfCountryRegion.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfCountryRegion.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }
    
    private func configurePhoneTextField(){
        
        self.tfPhoneNumber.delegate = self
        self.tfPhoneNumber.placeholder = "Enter Phone Number"
        self.tfPhoneNumber.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPhoneNumber.borderWidth = 1.5
        self.tfPhoneNumber.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        self.tfPhoneNumber.defaultBackgroundColor = .clear
        self.tfPhoneNumber.focusedBackgroundColor = .clear
        self.tfPhoneNumber.cornerRadius = 7
        self.tfPhoneNumber.inactiveHintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tfPhoneNumber.activeHintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        
    }
    
    //MARK:- TEXTFIELD ACTION
    
    @IBAction func countryDidBegin(_ sender: UITextField) {
        
        if sender == tfCountryRegion {
            countryBorderView.layer.borderWidth = 3
            countryBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            countryBorderView.layer.cornerRadius = 7
            countryBorderView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            
        }else {
            
            phoneBorderView.layer.borderWidth = 3
            phoneBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            phoneBorderView.layer.cornerRadius = 7
            phoneBorderView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            
        }
        
    }
    
    @IBAction func countryDidEnd(_ sender: UITextField) {
        if sender == tfCountryRegion{
            countryBorderView.layer.borderWidth = 0
            countryBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            countryBorderView.layer.cornerRadius = 7
            countryBorderView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        }else{
            phoneBorderView.layer.borderWidth = 0
            phoneBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            phoneBorderView.layer.cornerRadius = 7
            phoneBorderView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        }
       
        
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func countryButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CountryViewController")as! CountryViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        if AppUtility!.isEmpty(tfCountryRegion.text!){
            AppUtility?.showToast(string: "Enter a valid country region", view: self.view)
            
            return
        }
        
        if !AppUtility!.isValidPhoneNumber(strPhone: tfPhoneNumber.text!){
            AppUtility?.showToast(string: "Enter a valid phone number", view: self.view)
            return
        }
        
        let trimmedString = AppUtility?.validate(tfPhoneNumber.text!)
        
        let index = trimmedString!.index(trimmedString!.startIndex, offsetBy: 0)
        String(trimmedString![index])
        print( String(trimmedString![index]) )
        if String(trimmedString![index]) == "+"{
            phone_Number = trimmedString!
        }else{
            phone_Number = "+92\((trimmedString)!)"
        }
        
        changePhoneNoApi()
       
        
    }
    
    
    
    //MARK:- TEXTFIELD DELEGATES
    
    func textFieldDidChangeSelection(_ textField: UITextField) {

            if self.tfPhoneNumber.text != "" && self.tfCountryRegion.text != "" {
                
                if !AppUtility!.isValidPhoneNumber(strPhone: self.tfPhoneNumber.text!){
                    
                    btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                    return
                }
                
                btnContinue.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
            }else {
                btnContinue.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            }
           
    }
    
    //MARK:- CHANGE PHONE API
    
    private func changePhoneNoApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changePhoneNo(user_id: id, phone: phone_Number) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhoneNumberOtpViewController")as! PhoneNumberOtpViewController
                    isPhone = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }


}
