//
//  PhoneNumberOtpViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 16/08/2021.
//

import UIKit
import DPOTPView

class PhoneNumberOtpViewController: UIViewController {
    
    //MARK:- Outlet
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var otpView: DPOTPView!
    @IBOutlet weak var btnCallMe: UIButton!
    
    //MARK:- VARS
    
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    
    
    //MARK:- View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenDetection()
        otpView.dpOTPViewDelegate = self
        
    }
    //MARK:- SCREEN DETECTION
    
    private func screenDetection(){
        
        if isPhone == true {
            self.phoneNumberLabel.text = "We sent a code to \(phone_Number).Enter the code in that message."
            btnCallMe.isHidden = false
        }else {
            self.phoneNumberLabel.text = "We sent a code to \(email).Enter the code in that message."
            btnCallMe.isHidden = true
            
        }
        
        
    }
    
    
    
    //MARK:- Button Action
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func tryAgainButtonPressed(_ sender: UIButton) {
        
        if isPhone == true {
            updatePhoneNoApi()
            otpView.text = ""
            otpView.becomeFirstResponder()
            
        }else {
            updateEmailApi()
            otpView.text = ""
            otpView.becomeFirstResponder()
        }
        
        
        
        
    }
    
    @IBAction func callMeInsteadButtonPressed(_ sender: UIButton) {
        
        
    }
    
    //MARK:- VERIFY CHANGE EMAIL CODE
    
    private func verifyChangeEmailCodeApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyChangeEmailCode(new_email: email, code: otpView.text!, user_id: id) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let userObj = resp!["msg"] as! [String:Any]
                    UserObject.shared.Objresponse(response: userObj)
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: EditProfileViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    //MARK:- VERIFY CHANGE PHONE CODE
    
    private func verifyChangePhoneCodeApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.verifyPhoneNumber(phone: phone_Number, verify: "1", code: otpView.text!) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    if let userObj = resp!["msg"] as? [String:Any]{
                        UserObject.shared.Objresponse(response: userObj)
                    }
                   
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: EditProfileViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
    //MARK:- UPDATE EMAIL API
    
    private func updateEmailApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changeEmailAddress(email: email, user_id: id) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                   
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
    }
    
    //MARK:- UPDATE PHONE API
    
    private func updatePhoneNoApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.changePhoneNo(user_id: id, phone: phone_Number) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}


//MARK:- Textfield Delegate

extension PhoneNumberOtpViewController: DPOTPViewDelegate {
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        if isPhone == true {
            if position == 3 && text.count == 4 {
                otpView.dismissOnLastEntry = true
                verifyChangePhoneCodeApi()
                
            }
            
        }else {
            
            if position == 3 && text.count == 4 {
                otpView.dismissOnLastEntry = true
                verifyChangeEmailCodeApi()
                
            }
        }
        
        
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
        
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
        
    }
}
