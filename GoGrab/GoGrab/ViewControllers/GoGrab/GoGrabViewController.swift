//
//  GoGrabViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 19/05/2021.
//

import UIKit
import KWDrawerController

class GoGrabViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    @IBOutlet var viewShadow: [UIView]!
    
    //MARK:- Vars
    
    
    var arrOptions = [["name":"Car","img":"carRunnig"],
                      ["name":"Bike","img":"20-20"],
                      ["name":"Food","img":"food"],
                      ["name":"Delivery","img":"delivery"],
                      ["name":"City to City","img":"cityToCity"],
                      ["name":"Recharge","img":"recharge"]]
    var myUser: [User]? {didSet {}}
    var myCountry: [UserCountry]? {didSet {}}
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.optionsCollectionView.delegate = self
        self.optionsCollectionView.dataSource = self
        viewShadow.forEach { views in
            views.layer.cornerRadius = 5
            views.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            views.layer.shadowRadius = 5
            views.layer.shadowOpacity = 1
            views.layer.shadowOffset = CGSize.zero
            
        }
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        readDataFromArchive()
        
    }
    
    //MARK:- READ DATA
    
    private func readDataFromArchive(){
        self.myUser = User.readUserFromArchive()
        self.myCountry = UserCountry.readUserCountryFromArchive()
        if (myUser != nil && myUser?.count != 0) {
            id = myUser?[0].Id ?? ""
            
        }else{
           id = ""
        }
        if (myUser?.count != 0){
           
            print("idd:",myUser?[0].Id ?? "909")
            id = myUser?[0].Id ?? ""
            first_Name = myUser?[0].first_name ?? ""
            last_Name = myUser?[0].last_name ?? ""
            email = myUser?[0].email ?? ""
            username = (myUser?[0].first_name)! + (myUser?[0].last_name)!
            DOB = myUser?[0].dob ?? ""
            phone_Number = myUser?[0].phone ?? ""
            country_id = myUser?[0].country_id ?? ""
            gender = myUser?[0].gender ?? ""
            print("DOB",DOB)
            print("username",username)
        }

    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func selectDropOffPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WhereToViewController") as! WhereToViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- COLLECTION VIEW WITH DELEGATES


extension GoGrabViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize(width: (self.optionsCollectionView.frame.size.width - 60) / 3, height: (self.optionsCollectionView.frame.size.height - 30) / 2)
        return  cellSize
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GrabOptionsCVC", for: indexPath) as! GrabOptionsCVC
        cell.contentView.layer.cornerRadius = 5
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        cell.layer.shadowRadius = 3
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.masksToBounds = false
        
        cell.imgIcon.image = UIImage(named: "\(arrOptions[indexPath.row]["img"] ?? "food")")
        cell.lblname.text = arrOptions[indexPath.row]["name"]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "DrawerController") as! DrawerController
            isNowLaterShow = true
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "DrawerController") as! DrawerController
            isNowLaterShow = true
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else {
            
        }
       
    }
    
    
}
