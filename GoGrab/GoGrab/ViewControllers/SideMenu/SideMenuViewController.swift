//
//  SideMenuViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 02/06/2021.
//

import UIKit
import KWDrawerController

class SideMenuViewController: UIViewController {
    @IBOutlet weak var tblSideMenu: UITableView!
    
    var arrMenu = [["name":"Your rides","img":"CalenderTime"],
                   ["name":"Settings","img":"gear"]]
    override func viewDidLoad() {
        super.viewDidLoad()

        tblSideMenu.delegate = self
        tblSideMenu.dataSource = self
    }
    

 
}
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC", for: indexPath) as! SideMenuTVC
        cell.imgIcon.image = UIImage(named: "\(arrMenu[indexPath.row]["img"] ?? "CalenderTime")")
        cell.lblTitle.text = arrMenu[indexPath.row]["name"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "yourRidesMainViewController") as! yourRidesMainViewController
            vc.hidesBottomBarWhenPushed = true
            self.drawerController?.closeSide()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "WhereToViewController") as! WhereToViewController
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
