//
//  CuntactUsEmailViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 01/06/2021.
//

import UIKit

class ContactUsEmailViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UITextViewDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var attachTableView: UITableView!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var viewForText: UITextView!
    @IBOutlet weak var btnAttachImage: UIButton!
    
    //MARK:- Vars
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    let imgPickerController = UIImagePickerController()
    var imageArr = [String]()
    var objImage = [String:Any]()
    var img_Url = ""
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSendMessage.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
        btnAttachImage.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
        viewForText.delegate = self
        self.viewForText.layer.cornerRadius = 5
        self.viewForText.layer.borderWidth = 1
        attachTableView.delegate = self
        attachTableView.dataSource = self
        self.imgPickerController.delegate =  self
        attachTableView.tableFooterView = UIView()
    }
    
    //MARK:- BUTTON ACTION

    
    @IBAction func sendMessageButtonPressed(_ sender: UIButton) {
        if viewForText.text == "" || viewForText.text == nil {
            AppUtility?.showToast(string: "Enter your message", view: self.view)
            return
        }
        if imageArr.count == 0 {
            AppUtility?.showToast(string: "Enter your Attachment", view: self.view)
            return
            
        }
        contactUsApi()
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @objc func deleteImg(btn:UIButton){
        self.imageArr.remove(at: btn.tag)
        self.attachTableView.reloadData()
    }
    
    @IBAction func attachImageButtonPressed(_ sender: UIButton) {
        print("running")
        let tab = UIAlertController(title:"Please Select", message: nil, preferredStyle:
                                        .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (alert:UIAlertAction) in
            self.imgPickerController.sourceType = .camera
            self.present(self.imgPickerController, animated: true, completion: nil)
            
        }
        let PhotoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (alert:UIAlertAction) in
            self.imgPickerController.sourceType = .photoLibrary
            self.present(self.imgPickerController, animated: true, completion: nil)
            
        }
        let cancel = UIAlertAction(title: "Cancel", style:.cancel, handler: nil)
       
        tab.addAction(camera)
        tab.addAction(PhotoLibrary)
        tab.addAction(cancel)
        present(tab, animated: true, completion: nil)
    }
    
   
    //MARK:- TEXTFIELD DELEGATE
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if viewForText.text != "" {
         
            btnSendMessage.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
          
        }else {
            btnSendMessage.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
        }
        
    }
    
    //MARK:- TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.imageArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttachImageTableViewCell", for: indexPath)as! AttachImageTableViewCell
        cell.lblImageName.text = self.imageArr[indexPath.row] as! String
        cell.btnDelete.tag =  indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(self.deleteImg(btn:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    //MARK: Image Picker
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("didFinsih picker call")
      
        self.objImage.removeAll()
        self.imageArr.removeAll()
        
        
        if let pickedImage = info[.originalImage] as? UIImage {
            let imageData = pickedImage.jpegData(compressionQuality: 0.5)!.base64EncodedString()
            self.objImage = [
                "file_data" : imageData
            ]
            let number = Int.random(in: 0..<100)
           
            var imageName = "image.\(number).jpg"
            self.imageArr.append(imageName)
            self.attachTableView.reloadData()
            if imageArr.count == 0{
                btnAttachImage.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            }else {
                btnAttachImage.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
            }
            img_Url = imageData
            
            picker.dismiss(animated: true, completion: nil)
            
        }else{
            print("error")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- CONTACT US API
    
    private func contactUsApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.contactUs(user_id: id, message: viewForText.text, attachment: img_Url) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    let resObj = resp!["msg"] as! String
                    AppUtility?.showToast(string: resObj, view: self.view)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: ReportProblemViewController.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                    
                   
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
   
}
