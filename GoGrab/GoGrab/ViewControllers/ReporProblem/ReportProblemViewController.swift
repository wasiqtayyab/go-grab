//
//  ReportProblemViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 01/06/2021.
//

import UIKit

class ReportProblemViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tblProblems: UITableView!
    
    //MARK:- Vars
    
    var arrProblems = ["Captain caused the ride charges to increase","I paid Captain extra but the amount in not visible","I lost an item","Other"]
    
    
    //MARK:- VIEW DID LOAd
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblProblems.delegate = self
        tblProblems.dataSource = self
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- TABLE VIEW WITH DELEGATE

extension ReportProblemViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProblems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProblemReasonsTVC", for: indexPath) as! ProblemReasonsTVC
        cell.lblProblem.text = arrProblems[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 2 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ContactUsEmailViewController") as! ContactUsEmailViewController
            
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 3{
            let vc = storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    
}
