//
//  AttachImageTableViewCell.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 17/08/2021.
//

import UIKit

class AttachImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblImageName: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
