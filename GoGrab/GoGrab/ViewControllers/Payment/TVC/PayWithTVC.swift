//
//  PayWithTVC.swift
//  GoGrab
//
//  Created by Naqash Ali on 19/05/2021.
//

import UIKit

class PayWithTVC: UITableViewCell {
    
    @IBOutlet weak var payLabel: UILabel!
    @IBOutlet weak var paymentImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
