//
//  PaymentMethodsTVC.swift
//  GoGrab
//
//  Created by Naqash Ali on 02/06/2021.
//

import UIKit

class PaymentMethodsTVC: UITableViewCell {
    //MARK:- OUTLET
   
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
