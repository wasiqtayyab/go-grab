//
//  PaymentMethodsViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 02/06/2021.
//

import UIKit
import ActionSheet

class PaymentMethodsViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewAddPayment: UIView!
    @IBOutlet weak var btnAddPaymentMethod: UIButton!
    @IBOutlet weak var tableViewConstant: NSLayoutConstraint!
    
    //MARK:- VARS
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
    var brand_name = ""
    var index = 5
    var paymentArr = [[String:Any]]()
    var isEditRow = false
    let defaults = UserDefaults.standard
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        paymentTableView.tableFooterView = UIView()
        
    }
    
    //MARK:- VIEW WILL APPEAR
    
    override func viewWillAppear(_ animated: Bool) {
        isEditRow = false
        btnAddPaymentMethod.isHidden = false
        btnEdit.setTitle("Edit", for: .normal)
        self.viewAddPayment.isHidden = false
        showCardApi()
        print("paymentArr",paymentArr)
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        if isEditRow == false {
            self.isEditRow = true
            btnEdit.setTitle("Done", for: .normal)
        }else {
            self.isEditRow = false
            btnEdit.setTitle("Edit", for: .normal)
        }
        
    }
    
    
    @IBAction func addPaymentButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayWithViewController")as! PayWithViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

//MARK:- TABLE VIEW DELEGATE

extension PaymentMethodsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodsTVC", for: indexPath) as! PaymentMethodsTVC
        index = indexPath.row
        brand_name = paymentArr[indexPath.row]["brand"] as! String
        
        if brand_name == "Visa"{
            cell.cardImage.image = UIImage(named: "icons8-visa")
        }else if brand_name == "JCB"{
            cell.cardImage.image = UIImage(named: "icons8-jcb")
        }else {
            cell.cardImage.image = UIImage(named: "icons8-mastercard")
        }

        
        cell.cardNameLabel.text = "\((paymentArr[indexPath.row]["brand"])!)(\((paymentArr[indexPath.row]["last4"])!))"
        cell.expiryLabel.text = "\(paymentArr[indexPath.row]["exp_month"]!)/\(paymentArr[indexPath.row]["exp_year"]!)"
        cell.usernameLabel.text = paymentArr[indexPath.row]["name"] as! String
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if isEditRow == true {
            return true
        }else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let obj  =  self.paymentArr[indexPath.row]["PaymentCard"] as! [String:Any]
        card_id = obj["id"] as! String
        print("card_id",card_id)
        
        
        return UISwipeActionsConfiguration(actions: [
            makeDeleteContextualAction(forRowAt: indexPath)
        ])
    }
    
    //MARK: - Contextual Actions
    private func makeDeleteContextualAction(forRowAt indexPath: IndexPath) -> UIContextualAction {
        return UIContextualAction(style: .destructive, title: "Delete") { [self] (action, swipeButtonView, completion) in
            
            
            self.nativeClone()
            btnAddPaymentMethod.isHidden = true
            
            completion(true)
        }
    }
    
    
    func nativeClone(_ sender: Any? = nil) {
        let sheet = ActionSheet(title: "Remove this payment method?",
                                message: "Removing this card means you won't be able to use this payment method any more")
        
        sheet.addAction(location: .bottom, ActionButton(title: "Keep card on GoGrab", style: .prominent, options: [.fontTint : UIColor(named: "goGreen")!], handler: { [self] action in
           
            sheet.dismiss()
            btnAddPaymentMethod.isHidden = false
        }))
        
       
        sheet.addAction(location: .body, ActionButton(title: "Remove", style: .normal, options: [.fontTint : UIColor.red], handler: { [self] action in
            self.showAlert("Prominent button pressed.")
            
            sheet.dismiss()
            btnAddPaymentMethod.isHidden = false
        }))
        sheet.present(self)
    }
    
    func showAlert(_ title: String) {
        deleteCardApi()
       
    }
    
    
    //MARK:- DELETE CARD API
    
    private func deleteCardApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.deletePaymentCard(user_id: id, id: card_id) { [self] (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    self.paymentArr.remove(at: index)
                    self.paymentTableView.reloadData()
                    viewAddPayment.isHidden = true
                   
                }else{
                    viewAddPayment.isHidden = false
                    
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
    
    //MARK:- SHOW CARD API
    
    private func showCardApi(){
        self.loader.isHidden = false
        self.paymentArr.removeAll()
        ApiHandler.sharedInstance.showUserCards(user_id: id) { [self] (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    self.viewAddPayment.isHidden = true
                    
                    let userObj = resp!["msg"] as! [[String:Any]]
                    self.paymentArr = userObj
                    paymentTableView.delegate = self
                    paymentTableView.dataSource = self
                    
                    self.paymentTableView.reloadData()
                   
                   
                }else{
                    self.viewAddPayment.isHidden = false
                   
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
}
