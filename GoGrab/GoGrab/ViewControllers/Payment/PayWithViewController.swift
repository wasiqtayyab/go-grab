//
//  PayWithViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 19/05/2021.
//

import UIKit

class PayWithViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tblPayWith: UITableView!
    
    //MARK:- Vars
    
    let paymentArr = [["payment":"Credit or debit card","image":"104-104"],
               ["payment":"Paypal","image":"120-120"]]
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblPayWith.delegate = self
        tblPayWith.dataSource = self
    }
    
    //MARK:- BUTTON ACTION
  
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
}

//MARK:- TABLEVIEW DELEGATE

extension PayWithViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "PayWithTVC") as! PayWithTVC
        cell.payLabel.text = paymentArr[indexPath.row]["payment"]
        cell.paymentImage.image = UIImage(named: paymentArr[indexPath.row]["image"]!)
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "AddCardViewController")as! AddCardViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            print("visa")
        }
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
