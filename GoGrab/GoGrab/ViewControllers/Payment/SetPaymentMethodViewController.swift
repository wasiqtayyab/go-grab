//
//  SetPaymentMethodViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 01/06/2021.
//

import UIKit

class SetPaymentMethodViewController: UIViewController {

  
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func learnMoreButtonPressed(_ sender: UIButton) {
    }
    
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addPaymentMethodButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayWithViewController")as! PayWithViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
  
    
}
