//
//  AddCardViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 20/05/2021.
//

import UIKit
import CardScanner

class AddCardViewController: UIViewController,UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfCardNumber: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var tfExpiration: UITextField!
    @IBOutlet weak var tfCvv: UITextField!
    @IBOutlet weak var tfZipCode: UITextField!
    
    //MARK:- VARS
    private lazy var loader : UIView = {
        return (AppUtility?.createActivityIndicator(self.view))!
    }()
    let defaults = UserDefaults.standard
    var expire_Month = "01"
    var expire_Year = "21"
    var arrMonth = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    var years: [Int]!
    var pickerExpDate = UIPickerView()
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerExpDate.delegate = self
        pickerExpDate.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        textfieldDelegate()
        self.commonSetup()
        self.showDatePickerTo()
    }
    
    //MARK:- TEXTFIELD DELEGATE
    
    private func textfieldDelegate(){
        tfCardNumber.delegate = self
        tfExpiration.delegate = self
        tfCvv.delegate = self
        tfZipCode.delegate = self
    }
    
    //MARK:- COMMON SETUP
    
    func commonSetup() {
        var years: [Int] = []
        if years.count == 0 {
            var year = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year , from: NSDate() as Date)
            let string = "\(year)"
            
            let digits = string.compactMap{ $0.wholeNumberValue }
            var currentYear = Int("\(digits[2])\(digits[3])") ?? 10
            for _ in 1...10 {
                years.append(year)
                year += 1
               
                
            }
        }
        self.years = years
        
    }
    
    //MARK: - PICKERVIEW DELEGATE
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return arrMonth.count
        } else {
            return years.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return arrMonth[row]
        } else {
            return "\(years[row])"
        }
        
    }
     
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        var mnth = 0
        var yrs = 0
        if component == 0 {
            self.expire_Month = arrMonth[row]
            let expMonth = arrMonth[row]
            mnth = Int(expMonth) ?? 0
            
        } else if component == 1 {
            
            let string = "\(years[row])"
            let digits = string.compactMap{ $0.wholeNumberValue }
            
            self.expire_Year = "\(digits[2])\(digits[3])"
            let expYear = years[row]
            yrs = Int(expYear)
        }
       
          
    }
    
    //MARK:- DATE PICKER
    
    func showDatePickerTo(){
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerTo));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        self.tfExpiration.inputAccessoryView = toolbar
        self.tfExpiration.inputView = pickerExpDate
        
    }
    
    @objc func donedatePickerTo(){
        tfExpiration.text = "\(expire_Month)/\(expire_Year)"
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePickerTo(){
        self.view.endEditing(true)
    }
    
    
    
    
    //MARK:- CREDIT CARD EXPIRY DATE FORMATTER
    
    private func cardExpiryDateFormat(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yy"
        
        guard let enteredDate = dateFormatter.date(from: tfExpiration.text!) else {
            AppUtility?.showToast(string: "Invalid Expiry date", view: self.view)
            return
        }
        
        let endOfMonth = Calendar.current.date(byAdding: .month, value: 1, to: enteredDate)!
        let now = Date()
        if (endOfMonth < now) {
            print("Expired - \(enteredDate) - \(endOfMonth)")
            AppUtility?.showToast(string: "Your card is expired now", view: self.view)
            return
        } else {
            
            print("valid - now: \(now) entered: \(enteredDate)")
            
        }
        
    }
    
    //MARK:- CARD NUMBER VALIDATOR
    private func cardNumberValidator(){
        
        if CreditCardValidator(tfCardNumber.text!).isValid {
            
            if let type = CreditCardValidator(tfCardNumber.text!).type {
                print("type",type)
                
            } else {
                AppUtility?.showToast(string: "Card type is not found", view: self.view)
                return
            }
        } else {
            
            AppUtility?.showToast(string: "Enter a valid card number", view: self.view)
            return
        }
        
    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func cameraButtonPressed(_ sender: UIButton) {
        
        let scannerView = CardScanner.getScanner { [self] card, date  in
            tfCardNumber.text = card
            print("date",date)
            tfExpiration.text = date
//            let fullNameArr = tfExpiration.text?.components(separatedBy: "/")
//            expire_Month = fullNameArr![0] // First
//            expire_Year = fullNameArr![1] // Last

        }
        present(scannerView, animated: true, completion: nil)
        
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        if !CreditCardValidator(tfCardNumber.text!).isValid {
            AppUtility?.showToast(string: "Enter a valid card number", view: self.view)
            return
        }
        
        cardNumberValidator()
        cardExpiryDateFormat()
        addPaymentApi()
        
    }
    
   
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- TEXTFIELD ACTION
   
    @IBAction func cardNumberTextfieldPressed(_ sender: UITextField) {
        cardNumberValidator()
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        if textField == tfCardNumber {
            
            let currentText = tfCardNumber.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            if updatedText.count == 5 && string != "" {
                textField.text = textField.text! + " "
                return true
            } else if updatedText.count == 10 && string != ""{
                textField.text = textField.text! + " "
                return true
            }else if updatedText.count == 15 && string != ""{
                textField.text = textField.text! + " "
                return true
            }
            
            return updatedText.count <= 19
            
        }
        if textField == self.tfExpiration {
            
            let currentText = tfExpiration.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            if updatedText.count == 3 && string != "" {
                textField.text = textField.text! + "/"
                return true
            }
            
            return updatedText.count <= 5
        }
        
        
        if textField == self.tfCvv {
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
            
        }
        
        if textField == self.tfZipCode {
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if tfCardNumber.text != nil && tfExpiration.text != nil && tfCvv.text != nil && tfZipCode.text != nil {
            
            if !CreditCardValidator(tfCardNumber.text!).isValid {
                btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            if AppUtility!.isEmpty(tfCardNumber.text){
                btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            if AppUtility!.isEmpty(tfExpiration.text){
                btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            if AppUtility!.isEmpty(tfCvv.text){
                btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            if AppUtility!.isEmpty(tfZipCode.text){
                btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
                return
            }
            
            btnNext.backgroundColor = #colorLiteral(red: 0, green: 0.6941176471, blue: 0.3098039216, alpha: 1)
            
        }else {
            btnNext.backgroundColor = #colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1)
            
        }
    }
    
    //MARK:- ADD PAYMENT API
    
    private func addPaymentApi(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.addPaymentCard(user_id: id, strdefault: "0", name: username, card: tfCardNumber.text!, cvc: tfCvv.text!, exp_month: expire_Month, exp_year: expire_Year) { (isSuccess, resp) in
            self.loader.isHidden = true
            if isSuccess {
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: PaymentMethodsViewController.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}


