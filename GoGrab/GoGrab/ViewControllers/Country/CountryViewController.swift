//
//  CountryViewController.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 04/08/2021.
//

import UIKit

class CountryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchControllerDelegate, UISearchBarDelegate {
   

    //MARK:- OUTLET
    
    @IBOutlet weak var countryTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK:- Vars
    private lazy var loader : UIView = {
            return (AppUtility?.createActivityIndicator(self.view))!
        }()
        
   
    var data = [[String:Any]]()
    var filterdata = [[String:Any]]()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        countryTableView.tableFooterView = UIView()
        searchBar.delegate = self
        searchBar.tintColor = #colorLiteral(red: 0.3019999862, green: 0.5410000086, blue: 1, alpha: 1)
       
    }
    
    //MARK: - VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        getContries()
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK:- TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell", for: indexPath) as! CountryTableViewCell
        let objCountry =  filterdata[indexPath.row]["Country"] as! [String:Any]
        if filterdata.count != 0
        {
            
            cell.countryLabel.text = objCountry["name"] as! String
        }
        else{
            cell.countryLabel.text = objCountry["name"] as! String
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isEmail == true {
            
            let aCountry = filterdata[indexPath.row]["Country"] as! [String:Any]
            country_code = "\(aCountry["name"]  as! String) (+\(aCountry["phonecode"]  as! String))"
            country_id = "\(aCountry["id"]  as! String)"
            self.dismiss(animated: true, completion: nil)
            
        }else {
            
            let aCountry = filterdata[indexPath.row]["Country"] as! [String:Any]
            country_code1 = "+\(aCountry["phonecode"]  as! String)"
            country_id = "\(aCountry["id"]  as! String)"
            self.dismiss(animated: true, completion: nil)
            print("country_code1",country_code1)
            
        }
        
    }
    
    //MARK:- SEARCHBAR DELEGATE
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.filterdata.removeAll()
        
        for i in 0 ..< data.count{
            let obj = self.data[i]
            let countryObj = obj["Country"] as! [String:Any]
            let strName = countryObj["name"] as! String
            
            if strName.range(of: searchText, options: .caseInsensitive) != nil{
                self.filterdata.append(obj)
            }
            
        }
        
        countryTableView.reloadData()
        
        print("filterdata: ",filterdata)
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("Pressed")
        self.searchBar.text = nil
        getContries()
    }
    
    
    //MARK:- COUNTRY API
    
     private func getContries(){
        self.loader.isHidden = false
        ApiHandler.sharedInstance.showCountries { (isSuccess, resp) in
            if isSuccess {
                self.loader.isHidden = true
                print(resp as Any)
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    //Way 1
                    if let obj  = resp?.value(forKey: "msg") as? [[String:Any]]{
                        self.data =  obj
                        self.filterdata = self.data
                        self.countryTableView.reloadData()
                    }
                }else{
                    AppUtility?.displayAlert(title: "Error", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
                
            }else{
                print(resp as Any)
            }
        }
        
    }
    
}
