//
//  CaptainOnWayViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 21/05/2021.
//

import UIKit

class CaptainOnWayViewController: UIViewController {

    @IBOutlet weak var viewCarNumber: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewCarNumber.layer.cornerRadius = 5
        
        self.viewCarNumber.layer.borderWidth = 1
        self.viewCarNumber.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selfDismiss(notification:)), name: Notification.Name("cancelCaptainOnWay"), object: nil)
    }
    

    @IBAction func cancelPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CancelRideViewController") as! CancelRideViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.comeFrom = "captainOnWay"
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func getSupportPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier:"RateRideViewController") as! RateRideViewController
        
        
        
     
     
     
                          let presentingVC = self.presentingViewController
                          self.dismiss(animated: false, completion: { () -> Void   in
                          
                            vc.modalPresentationStyle = .overCurrentContext
                          //  self.present(vc, animated: true, completion: nil)
                            presentingVC!.present(vc, animated: true, completion: nil)
                          })
    }
    @objc func selfDismiss(notification: Notification) {
     
        
        let vc = storyboard?.instantiateViewController(withIdentifier:"ReasonOfCancelViewController") as! ReasonOfCancelViewController
                          let presentingVC = self.presentingViewController
                          self.dismiss(animated: false, completion: { () -> Void   in
                          
                            vc.modalPresentationStyle = .overCurrentContext
                          //  self.present(vc, animated: true, completion: nil)
                              presentingVC!.present(vc, animated: true, completion: nil)
                          })
    }
    
    @IBAction func chatPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.modalPresentationStyle = .overCurrentContext
     
        self.present(vc, animated: true, completion: nil)
    }
    

}
