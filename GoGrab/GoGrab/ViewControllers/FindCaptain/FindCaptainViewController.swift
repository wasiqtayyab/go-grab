//
//  FindCaptainViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 21/05/2021.
//

import UIKit
import FloatingPanel

class FindCaptainViewController: UIViewController, FloatingPanelControllerDelegate {

    var fpc: FloatingPanelController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        fpc = FloatingPanelController()
        fpc.delegate = self
        
        let appearance = SurfaceAppearance()
        appearance.cornerRadius = 12.0
        fpc.surfaceView.appearance = appearance
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selfDismiss(notification:)), name: Notification.Name("cancelFindingCaptain"), object: nil)
    }
    
    @IBAction func getSupportPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier:"CaptainOnWayViewController") as! CaptainOnWayViewController
        fpc.set(contentViewController: vc)
        fpc.track(scrollView: vc.scrollView)
        fpc.isRemovalInteractionEnabled = false // Optional: Let it removable by a swipe-down

       //self.present(fpc, animated: true, completion: nil)
     
                          let presentingVC = self.presentingViewController
                          self.dismiss(animated: false, completion: { () -> Void   in
                          
                            vc.modalPresentationStyle = .overCurrentContext
                          //  self.present(vc, animated: true, completion: nil)
                            presentingVC!.present(self.fpc, animated: true, completion: nil)
                          })
        
        
        
    }
    
    
    @IBAction func rideCancelPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CancelRideViewController") as! CancelRideViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.comeFrom = "FindingCaptain"
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    @objc func selfDismiss(notification: Notification) {
     
        
        let vc = storyboard?.instantiateViewController(withIdentifier:"ReasonOfCancelViewController") as! ReasonOfCancelViewController
                          let presentingVC = self.presentingViewController
                          self.dismiss(animated: false, completion: { () -> Void   in
                          
                            vc.modalPresentationStyle = .overCurrentContext
                          //  self.present(vc, animated: true, completion: nil)
                              presentingVC!.present(vc, animated: true, completion: nil)
                          })
    }
}
