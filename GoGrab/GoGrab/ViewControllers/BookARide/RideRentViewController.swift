//
//  RideRentViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 22/05/2021.
//

import UIKit
import KWDrawerController
import GoogleMaps
import CoreLocation
import Alamofire

class RideRentViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    // MARK:-  IB Outlets
    
    @IBOutlet weak var tblDropSuggestion: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnNow: UIButton!
    @IBOutlet weak var btnLater: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var viewTopRideRent: UIView!
    @IBOutlet weak var viewBottomNowLater: UIView!
    @IBOutlet weak var contViewBottomNowHeight: NSLayoutConstraint!
    @IBOutlet weak var containerLater: UIView!
    @IBOutlet weak var containerViewLocationPicker: UIView!
    @IBOutlet weak var containerSelectRide: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    // MARK:-  Variales
    var comeFrom = ""
    var nowOrLater = "now"
    var locationManager = CLLocationManager()
    var strLat = ""
    var strLong = ""
    var strCity = ""
    var strCountry = ""
    // MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblDropSuggestion.delegate = self
        tblDropSuggestion.dataSource = self
        
        self.setupView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSelectRideContainer(notification:)), name: Notification.Name("pickUpConfirmed"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBookRide(notification:)), name: Notification.Name("bookingConfirmed"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selfPop(notification:)), name: Notification.Name("dismiss"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.laterScheduled(notification:)), name: Notification.Name("laterAsigned"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("later"), object: nil)

        
        print("comeFrom",comeFrom)
        
    }
    
    
    
    // MARK:-  IB Actions
    
    @IBAction func backPressed(_ sender: UIButton) {
        if self.nowOrLater == "later"{
            self.viewBottomNowLater.isHidden = false
            self.containerLater.isHidden = true
            self.btnNow.layer.borderWidth = 1
            self.btnNow.setTitleColor(#colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1), for: .normal)
            
            
            self.btnLater.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.btnLater.layer.borderWidth = 0
            self.nowOrLater = "now"
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    @IBAction func nowLaterPressed(_ sender: UIButton) {
        if sender.currentTitle == "Now" {
            
            self.btnNow.layer.borderWidth = 1
            self.btnNow.setTitleColor(#colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1), for: .normal)
            btnLater
                .setTitle("Later", for: .normal)
            
            self.btnLater.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.btnLater.layer.borderWidth = 0
        }else{
            self.btnNow.layer.borderWidth = 0
            self.btnNow.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            
            
            self.btnLater.setTitleColor(#colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1), for: .normal)
            self.btnLater.layer.borderWidth = 1
            
            self.viewBottomNowLater.isHidden = true
            self.containerLater.isHidden = false
            

            self.nowOrLater = "later"
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        let obj = notification.object as! [String]
        print("obj",obj)
        if obj[1] == "false" {
            self.viewBottomNowLater.isHidden = true
            self.containerLater.isHidden = false
        }else {
            self.viewBottomNowLater.isHidden = false
            self.containerLater.isHidden = true
            btnLater
                .setTitle(obj[0], for: .normal)
        }
    }
    
    
    @IBAction func menuPressed(_ sender: UIButton) {
        
        self.drawerController?.openSide(.right)
    }
    
    @IBAction func whereToPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WhereToViewController") as! WhereToViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK:-  Functions
    
    func checkLocationEnabled(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
                
            @unknown default:
                print("Permission not granted yet")
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.strLat = "\(locValue.latitude)"
        self.strLong = "\(locValue.longitude)"
        print("lat \(strLat)")
        print("lat \(strLong)")
        
        self.locationManager.stopUpdatingLocation()
        let camera = GMSCameraPosition.camera(withLatitude: Double("\(strLat)")!, longitude: Double("\(strLong)")!, zoom: 16.0)
        self.mapView.camera = camera
        
        let center = CLLocationCoordinate2D(latitude: Double("\(strLat)")!, longitude: Double("\(strLong)")!)
        self.mapView.animate(toLocation: center)
        
//        getAddressFromLatLong(latitude: Double("\(strLat)")!, longitude: Double("\(strLong)")!)
//
    }
    
    
//    func getAddressFromLatLong(latitude: Double, longitude : Double) {
//        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(GOOGLE_MAP_KEY)"
//
//        AF.request(url).responseJSON { response in
//            switch response.result {
//            case .success:
//                print("response",response.result)
//
//
//
//                if let results = response[""] as? [NSArray] {
//                    if results.count > 0 {
//                        if let addressComponents = results[0]["address_components"]! as? [NSArray] {
//                            let formatted_address = (results[0]["formatted_address"] as? String) ?? " "
//                            for component in addressComponents {
//                                if let temp = component.object(forKey: "types") as? [String] {
//
//                                    if (temp[0] == "locality") {
//                                        self.strCity =  (component["long_name"] as? String) ?? " "
//                                    }
//                                    if (temp[0] == "administrative_area_level_2") {
//                                        self.strCity = (component["long_name"] as? String) ?? " "
//                                    }
//                                    if (temp[0] == "administrative_area_level_1") {
//                                        let state =  component["long_name"] as? String
//                                    }
//                                    if (temp[0] == "country") {
//                                        self.strCountry = (component["long_name"] as? String) ?? " "
//                                    }
//                                }
//                            }
//                        }
//
//                    }
//                }
//            case .failure(let error):
//                print(error)
//            }
//        }
//
//    }
//
    
    func setupView(){
        self.containerLater.isHidden = true
        self.mapView.delegate = self
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        checkLocationEnabled()
        
        
        
        self.containerSelectRide.isHidden = true
        if isNowLaterShow == true {
            self.mapView.isMyLocationEnabled =  true
            self.containerViewLocationPicker.isHidden = true
            self.viewBottomNowLater.isHidden = false
            self.contViewBottomNowHeight.constant = 400
            self.viewTopRideRent.isHidden = false
        }else{
            self.containerViewLocationPicker.isHidden = false
            self.viewBottomNowLater.isHidden = true
            self.contViewBottomNowHeight.constant = 0
            self.viewTopRideRent.isHidden = true
        }
        self.btnNow.layer.cornerRadius = 5
        self.btnNow.layer.borderColor  = #colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1)
        self.btnNow.layer.borderWidth = 1
        
        
        self.btnLater.layer.cornerRadius = 5
        self.btnLater.layer.borderColor  = #colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1)
        self.btnLater.layer.borderWidth = 0
    }
    
    @objc func showSelectRideContainer(notification: Notification) {
        self.containerViewLocationPicker.isHidden = true
        self.containerSelectRide.isHidden = false
        self.btnBack.isHidden = false
        self.btnMenu.isHidden = false
        self.drawerController?.setAbsolute(true, for: .right)
        
    }
    
    @objc func onBookRide(notification: Notification) {
        self.containerViewLocationPicker.isHidden = true
        self.containerSelectRide.isHidden = true
        self.btnBack.isHidden = true
        self.btnMenu.isHidden = true
        self.drawerController?.setAbsolute(false, for: .right)
    }
    
    @objc func rideCanceled(notification: Notification) {
        self.containerViewLocationPicker.isHidden = true
        self.containerSelectRide.isHidden = true
        self.btnBack.isHidden = true
        self.btnMenu.isHidden = true
        self.drawerController?.setAbsolute(false, for: .right)
    }
    
    @objc func selfPop(notification: Notification) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func laterScheduled(notification: Notification) {
        self.btnNow.layer.borderWidth = 0
        self.btnNow.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        
        
        self.btnLater.setTitleColor(#colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1), for: .normal)
        self.btnLater.layer.borderWidth = 1
        
        self.viewBottomNowLater.isHidden = false
        self.containerLater.isHidden = true
        
        self.nowOrLater = "later"
    }
    
    
    
    
}



// MARK:-  TableView Extension
extension RideRentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "DropSuggestionTVC", for: indexPath) as! DropSuggestionTVC
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.containerViewLocationPicker.isHidden = false
        self.viewBottomNowLater.isHidden = true
        self.contViewBottomNowHeight.constant = 0
        
        UIView.animate(withDuration: 0.5) {
            self.viewTopRideRent.isHidden = true
        }
    }
    
}
