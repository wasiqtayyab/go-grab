//
//  SelectRideViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 24/05/2021.
//

import UIKit
import FloatingPanel


class SelectRideViewController: UIViewController, FloatingPanelControllerDelegate {
   
    

    @IBOutlet weak var viewGrabCar: UIView!
    @IBOutlet weak var viewGrabBike: UIView!
  
    var fpc: FloatingPanelController!

   
   
    override func viewDidLoad() {
        super.viewDidLoad()

        fpc = FloatingPanelController()
        fpc.delegate = self
        
        let appearance = SurfaceAppearance()
        appearance.cornerRadius = 12.0
        fpc.surfaceView.appearance = appearance
       
        self.viewGrabCar.layer.cornerRadius = 5
        self.viewGrabCar.layer.borderColor = #colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1)
        self.viewGrabCar.layer.borderWidth = 1
        self.viewGrabBike.layer.cornerRadius = 5
    }
    
    
    // MARK:-  Actions
    
    @IBAction func payWithPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SelectPayWithViewController") as! SelectPayWithViewController
    
       // vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func viewAllPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AllTypesRidesViewController") as! AllTypesRidesViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func promoPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddPromoViewController") as! AddPromoViewController
    
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func BookPressed(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("bookingConfirmed"), object: nil)
  
       
    
        
        let contentVC = storyboard?.instantiateViewController(withIdentifier: "FindCaptainViewController") as! FindCaptainViewController
        fpc.set(contentViewController: contentVC)

        fpc.isRemovalInteractionEnabled = false // Optional: Let it removable by a swipe-down

       
        self.present(fpc, animated: true, completion: nil)
     
    
        
        
    }
    

}
