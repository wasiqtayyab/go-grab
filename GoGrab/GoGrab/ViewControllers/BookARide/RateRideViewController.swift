//
//  RateRideViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 26/05/2021.
//

import UIKit
import Cosmos

class RateRideViewController: UIViewController {

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewEarnedPoints: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var viewRatingCosom: CosmosView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnDone.layer.cornerRadius = 5
        self.viewMain.sendSubviewToBack(self.btnDone)
        self.viewEarnedPoints.layer.cornerRadius = 5
        self.viewEarnedPoints.layer.borderWidth = 1
        self.viewEarnedPoints.layer.borderColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 0.5544674296)
        
        self.viewBottom.layer.cornerRadius = 5
        self.viewBottom.layer.borderWidth = 1
        self.viewBottom.layer.borderColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 0.5544674296)
        
        viewRatingCosom.didTouchCosmos = { rating in
            self.viewMain.sendSubviewToBack(self.viewEarnedPoints)
            print("touched")
        }
    }
    

    @IBAction func donePressed(_ sender: UIButton) {

//        let vc = storyboard?.instantiateViewController(withIdentifier:"TabbarVC") as! TabbarVC
//
//        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.pushViewController(vc, animated: true)
        self.dismiss(animated: true, completion: { () -> Void   in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismiss"), object: nil)
              })
    }
    

}
