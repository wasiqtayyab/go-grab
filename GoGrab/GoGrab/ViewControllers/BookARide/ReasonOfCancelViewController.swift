//
//  ReasonOfCancelViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 04/06/2021.
//

import UIKit

class ReasonOfCancelViewController: UIViewController {
    @IBOutlet weak var tblReasons: UITableView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var contHeightTblView: NSLayoutConstraint!
    
    var arrReasons = [["reason":"I don't need a ride anymore","isSelected":"1"],
                      ["reason":"I want to change my bookings details","isSelected":"0"],
                      ["reason":"It took too long to find a captain","isSelected":"0"],
                      ["reason":"Other","isSelected":"0"]]
    override func viewDidLoad() {
        super.viewDidLoad()

        tblReasons.delegate = self
        tblReasons.dataSource = self
        
        self.contHeightTblView.constant = CGFloat(arrReasons.count * 35)
 
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            self.view.layoutIfNeeded()
        }
       
    }
    
    @IBAction func submitPressed(_ sender: UIButton) {
       
        NotificationCenter.default.post(name: Notification.Name("pickUpConfirmed"), object: nil)
        self.dismiss(animated: true, completion: nil)

    }
    
   

}
extension ReasonOfCancelViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return arrReasons.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReasonOfCancelTVC", for: indexPath) as! ReasonOfCancelTVC
        if arrReasons[indexPath.row]["isSelected"] == "1"{
            cell.imgSelected.image = UIImage(systemName: "checkmark.circle.fill")
            cell.imgSelected.tintColor = #colorLiteral(red: 0.2160000056, green: 0.7099999785, blue: 0.3059999943, alpha: 1)
        }else{
            cell.imgSelected.image = UIImage(systemName: "circle")
            cell.imgSelected.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
        cell.lblReason.text = arrReasons[indexPath.row]["reason"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (arrReasons[indexPath.row]["isSelected"]! == "0" )  {
            for i in 0..<self.arrReasons.count{
                var obj  = self.arrReasons[i]
                obj.updateValue("0", forKey:"isSelected")
                self.arrReasons.remove(at: i)
                self.arrReasons.insert(obj, at: i)
            }
            var obj   =  self.arrReasons[indexPath.row]
            obj.updateValue("1", forKey: "isSelected")
            self.arrReasons.remove(at: indexPath.row)
            self.arrReasons.insert(obj, at: indexPath.row)
        }
        self.tblReasons.reloadData()
    }
    
    
}
