//
//  WhereToViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 03/06/2021.
//

import UIKit

class WhereToViewController: UIViewController {
    
    @IBOutlet weak var whereToView: UIView!
    @IBOutlet weak var tblRecentLocations: UITableView!
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var locationConst: NSLayoutConstraint!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK:- FUNCTION
    
    func setupView(){
        
        tblRecentLocations.delegate = self
        tblRecentLocations.dataSource = self
        
        whereToView.layer.shadowColor = UIColor.systemGray6.cgColor
        whereToView.layer.shadowOffset = CGSize(width: 0, height: 3.0)
        whereToView.layer.shadowOpacity = 10.0
        whereToView.layer.shadowRadius = 0.0
        whereToView.layer.masksToBounds = false
        locationView.layer.cornerRadius = 5.0
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.locationView.isUserInteractionEnabled =  true
        self.locationView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        
        
        self.locationView.addGestureRecognizer(swipeDown)
    }
    
    //MARK:- VIEW ACTION
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) {
        
        if gesture.direction == .up {
            
            UIView.animate(withDuration: 2) {
                self.locationConst.constant = 2
            }
            
        }
        else if gesture.direction == .down {
            UIView.animate(withDuration: 2) {
                self.locationConst.constant = 460
            }
        }
    }
    
}

//MARK:- TABLE VIEW
extension WhereToViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WhereToViewTableViewCell", for: indexPath)as! WhereToViewTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        
        let label = UILabel()
        
        label.text = "Recent Locations"
        label.font = UIFont(name: "AirbnbCerealApp-Medium", size: 14.0)
        headerView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20).isActive = true
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
}
