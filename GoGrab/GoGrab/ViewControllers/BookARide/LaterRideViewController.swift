//
//  LaterRideViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 15/06/2021.
//

import UIKit

class LaterRideViewController: UIViewController {
    
    @IBOutlet weak var pickerDateTime: UIDatePicker!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var alertHeightConst: NSLayoutConstraint!
    @IBOutlet weak var lblScheduleDate: UILabel!
    @IBOutlet weak var imgAlert: UIImageView!
    @IBOutlet weak var lblAlert: UILabel!
    
    
    let dateFormatter = DateFormatter()
    var isValueChanged = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
       
    }
    
    func setupView(){
        pickerDateTime.minimumDate = Date()
        alertHeightConst.constant = 0
        
       
        dateFormatter.dateFormat = "E d MMM h:mm a"
        lblScheduleDate.text = dateFormatter.string(from: pickerDateTime.date)
    }
    
   
    
    @IBAction func valueChanged(_ sender: UIDatePicker) {
        lblScheduleDate.text = dateFormatter.string(from: pickerDateTime.date)
    
    }
    
    @IBAction func schedulePressed(_ sender: UIButton) {
        let datePicker = pickerDateTime.date

        let calendar = Calendar.current
        let timeComponents = calendar.dateComponents([.hour, .minute], from: datePicker)
        let nowComponents = calendar.dateComponents([.hour, .minute], from: Date())

        let difference = calendar.dateComponents([.minute], from: nowComponents, to: timeComponents).minute!
        print("difference",difference)
        if difference <= 30 {
            imgAlert.isHidden = false
            lblAlert.isHidden = false
            alertHeightConst.constant = 20
            lblAlert.text = "Please select time 30 min after current time"
           
            return
        }else {
            lblAlert.isHidden = true
            imgAlert.isHidden = true
            alertHeightConst.constant = 0
            let arr = [lblScheduleDate.text!,"true"] as [String]
            NotificationCenter.default.post(name: Notification.Name("later"), object: arr)

            
        }
    }
    
   

    
    
}
