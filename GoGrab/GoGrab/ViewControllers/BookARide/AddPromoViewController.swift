//
//  AddPromoViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 25/05/2021.
//

import UIKit

class AddPromoViewController: UIViewController {

    @IBOutlet weak var viewBackTF: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewBackTF.layer.cornerRadius = 5
        self.viewBackTF.layer.borderWidth = 1
        self.viewBackTF.layer.borderColor = #colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1)
    }
    
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
