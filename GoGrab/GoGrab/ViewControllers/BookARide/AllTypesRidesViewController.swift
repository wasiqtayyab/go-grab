//
//  AllTypesRidesViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 24/05/2021.
//

import UIKit

class AllTypesRidesViewController: UIViewController {
    @IBOutlet weak var tblRideTypes: UITableView!
    
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnEconomy: UIButton!
    
    
    var isSelected = "plus"
    override func viewDidLoad() {
        super.viewDidLoad()

        tblRideTypes.delegate = self
        tblRideTypes.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func plusPressed(_ sender: UIButton) {
        self.btnPlus.backgroundColor = .systemGray6
        self.btnPlus.setTitleColor(#colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1), for: .normal)
        self.btnEconomy.backgroundColor = .white
        self.btnEconomy.setTitleColor(.lightGray, for: .normal)
        
     //   self.tblRideTypes.moveSection(1, toSection: 0)
        let indexPath = IndexPath(item: 0, section: 0)
        self.tblRideTypes.scrollToRow(at: indexPath, at: .top, animated: true)
       // self.tblRideTypes.reloadData()
    }
    
    @IBAction func economyPressed(_ sender: UIButton) {
        self.btnPlus.backgroundColor = .white
        self.btnPlus.setTitleColor(.lightGray, for: .normal)
        self.btnEconomy.backgroundColor = .systemGray6
        self.btnEconomy.setTitleColor(#colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1), for: .normal)
       // self.tblRideTypes.moveSection(1, toSection: 0)
        let indexPath = IndexPath(item: 0, section: 1)
        self.tblRideTypes.scrollToRow(at: indexPath, at: .top, animated: true)
      //  self.tblRideTypes.reloadData()
    }
    
  
    

}
extension AllTypesRidesViewController: UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 5
            
        }else{
            return 10
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideTypesTVC", for:indexPath) as! RideTypesTVC
        if indexPath.section == 0 {
      
            if indexPath.row != 0{
            cell.viewCellBack.backgroundColor = .white
        }
        }else{
            cell.viewCellBack.backgroundColor = .white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            return 50
        
        
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
    
            return 50
       
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if #available(iOS 13.0, *) {
            headerView.backgroundColor = .systemBackground
        } else {
            headerView.backgroundColor = .white
        }
        let label = UILabel()
        label.frame = CGRect.init(x: 20, y: 0, width: headerView.frame.width-10, height: headerView.frame.height)
        
        label.font = UIFont(name: "AirbnbCerealApp-Medium", size: 15.0 )// my custom font
    
        label.textColor = .black
        
        
        headerView.addSubview(label)
        
        
        if section == 0 {
            label.text = "Plus"
            let btn: UIButton = UIButton(frame: CGRect.init(x: headerView.frame.size.width - 50, y: (headerView.frame.size.height / 2) - 30 , width: 30, height: headerView.frame.height)) //frame.size.width - 60
                    //  btn.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
                      btn.tag = section
                    //  btn.layer.cornerRadius = 7.0
        
           // btn.contentHorizontalAlignment = .right
            btn.setImage(UIImage(systemName: "info.circle"), for: .normal)
            btn.tintColor = #colorLiteral(red: 0.7099999785, green: 0.7099999785, blue: 0.7099999785, alpha: 1)
            headerView.addSubview(btn)
        }else{
            label.text = "Economy"
            let btn: UIButton = UIButton(frame: CGRect.init(x: headerView.frame.size.width - 50, y: headerView.frame.midY - 30 , width: 30, height: headerView.frame.height)) //frame.size.width - 60
                    //  btn.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
                      btn.tag = section
                    //  btn.layer.cornerRadius = 7.0
        
           // btn.contentHorizontalAlignment = .right
            btn.setImage(UIImage(systemName: "info.circle"), for: .normal)
            btn.tintColor = #colorLiteral(red: 0.7099999785, green: 0.7099999785, blue: 0.7099999785, alpha: 1)
            headerView.addSubview(btn)
        }
        return headerView
    }
    
    
}
