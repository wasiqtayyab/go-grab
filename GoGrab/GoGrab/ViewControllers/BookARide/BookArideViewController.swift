//
//  BookArideViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 22/05/2021.
//

import UIKit

class BookArideViewController: UIViewController {
// MARK:-  IB Outlets
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var viewForAddNotes: UIView!
    @IBOutlet weak var contViewNoteHeight: NSLayoutConstraint!
    @IBOutlet weak var lblLocName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    
    // MARK:-  Variables
    var comeFrom = ""
    var isDropoffAdded = false
    
    
    // MARK:-  ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.contViewNoteHeight.constant = 0
        self.viewForAddNotes.isHidden = true
        
        
        self.viewForAddNotes.layer.cornerRadius = 5
        self.viewForAddNotes.layer.borderColor = #colorLiteral(red: 0.2150000036, green: 0.7080000043, blue: 0.3070000112, alpha: 1)
        self.viewForAddNotes.layer.borderWidth = 1
        
       
    }
    // MARK:-  ViewWillApear
    override func viewWillAppear(_ animated: Bool) {
        if comeFrom == "bookAride"{
            
        }else{
            
        }
    }
    
   
    // MARK:-  IB Actions
    @IBAction func saveLocPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddToSvedPlacesViewController") as! AddToSvedPlacesViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func confirmPressed(_ sender: UIButton) {
        if isDropoffAdded == false {
            UIView.animate(withDuration: 0.2) {
                self.contViewNoteHeight.constant = 65
                self.viewForAddNotes.isHidden = false
                self.btnConfirm.setTitle("Confirm pick-up", for: .normal)
                self.view.layoutIfNeeded()
            }
           

            self.isDropoffAdded = true
        }else {
            NotificationCenter.default.post(name: Notification.Name("pickUpConfirmed"), object: nil)
        }
       
    }
    
    @IBAction func locationPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WhereToViewController") as! WhereToViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

   
    
}
