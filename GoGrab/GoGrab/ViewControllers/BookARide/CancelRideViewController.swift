//
//  CancelRideViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 02/06/2021.
//

import UIKit

class CancelRideViewController: UIViewController {

    
    var comeFrom = "FindingCaptain"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            self.view.layoutIfNeeded()
        }
       
    }
    
    @IBAction func dontCancelPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancelPressed(_ sender: UIButton) {

        
        if self.comeFrom == "FindingCaptain"{
            self.dismiss(animated: true, completion: { () -> Void   in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelFindingCaptain"), object: nil)
                 })
        }else{
            self.dismiss(animated: true, completion: { () -> Void   in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelCaptainOnWay"), object: nil)
                  })
        }
        
       
       
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
