//
//  ReasonOfCancelTVC.swift
//  GoGrab
//
//  Created by Naqash Ali on 04/06/2021.
//

import UIKit

class ReasonOfCancelTVC: UITableViewCell {

    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var lblReason: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
