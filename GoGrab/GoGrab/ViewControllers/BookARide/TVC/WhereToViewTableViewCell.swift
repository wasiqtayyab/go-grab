//
//  WhereToViewTableViewCell.swift
//  GoGrab
//
//  Created by Wasiq Tayyab on 05/11/2021.
//

import UIKit

class WhereToViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var lblDesLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
