//
//  SelectPayWithViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 15/06/2021.
//

import UIKit

class SelectPayWithViewController: UIViewController {
    @IBOutlet weak var tblPayWith: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblPayWith.delegate = self
        tblPayWith.dataSource = self
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    

  

}

extension SelectPayWithViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "PayWithTVC") as! PayWithTVC
        return cell
        
    }
    
    
}

