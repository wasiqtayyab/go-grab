//
//  ChatViewController.swift
//  GoGrab
//
//  Created by Naqash Ali on 25/06/2021.
//

import UIKit

class ChatViewController: UIViewController {

    @IBOutlet weak var viewChatTextView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewChatTextView.layer.borderColor = #colorLiteral(red: 0.7099999785, green: 0.7099999785, blue: 0.7099999785, alpha: 1)
        self.viewChatTextView.layer.borderWidth =  1
    }
    

    @IBAction func cancelPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}
