//
//  UserCountry.swift
//  GrabMyTaxiDriver
//
//  Created by mac on 10/01/2021.
//  Copyright © 2021 Dianosoft. All rights reserved.
//

import UIKit

class UserCountry: NSObject,NSCoding {
    
    var id : String?
    var iso : String?
    var name : String?
    var country : String?
    var iso3 : String?
    var phonecode : String?
    var country_code : String?
    var currency : String?
    var currency_symbol : String?
    var active : String?
    
    var capital:String?
    var created_at : String?
    var emoji : String?
    var emojiU : String?
    var flag : String?
    var native: String?
    var region : String?
    var short_name : String?
    var subregion : String?
    var updated_at :String?
    var wikiDataId : String?
    
    
    override init(){
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.iso, forKey: "iso")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.country, forKey: "country")
        aCoder.encode(self.iso3, forKey: "iso3")
        aCoder.encode(self.phonecode, forKey: "phonecode")
        aCoder.encode(self.country_code, forKey: "country_code")
        aCoder.encode(self.currency, forKey: "currency")
        aCoder.encode(self.currency_symbol, forKey: "currency_symbol")
        aCoder.encode(self.active, forKey: "active")
        
        aCoder.encode(self.capital, forKey: "capital")
        aCoder.encode(self.created_at, forKey: "created_at")
        aCoder.encode(self.emoji, forKey: "emoji")
        aCoder.encode(self.emojiU, forKey: "emojiU")
        aCoder.encode(self.flag, forKey: "flag")
        aCoder.encode(self.native, forKey: "native")
        aCoder.encode(self.region, forKey: "region")
        aCoder.encode(self.short_name, forKey: "short_name")
        aCoder.encode(self.subregion, forKey: "subregion")
        aCoder.encode(self.updated_at, forKey: "updated_at")
        aCoder.encode(self.wikiDataId, forKey: "wikiDataId")
     
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.iso = aDecoder.decodeObject(forKey: "iso") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.country = aDecoder.decodeObject(forKey: "country") as? String
        self.iso3 = aDecoder.decodeObject(forKey: "iso3") as? String
        self.phonecode = aDecoder.decodeObject(forKey: "phonecode") as? String
        self.country_code = aDecoder.decodeObject(forKey: "country_code") as? String
        self.currency = aDecoder.decodeObject(forKey: "currency") as? String
        self.currency_symbol = aDecoder.decodeObject(forKey: "currency_symbol") as? String
        self.active = aDecoder.decodeObject(forKey: "active") as? String
        
        self.capital = aDecoder.decodeObject(forKey: "capital") as? String
        self.created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        self.emoji = aDecoder.decodeObject(forKey: "emoji") as? String
        self.emojiU = aDecoder.decodeObject(forKey: "emojiU") as? String
        self.flag = aDecoder.decodeObject(forKey: "flag") as? String
        self.native = aDecoder.decodeObject(forKey: "native") as? String
        self.region = aDecoder.decodeObject(forKey: "region") as? String
        
        self.short_name = aDecoder.decodeObject(forKey: "short_name") as? String
        self.subregion = aDecoder.decodeObject(forKey: "subregion") as? String
        self.updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        self.wikiDataId = aDecoder.decodeObject(forKey: "wikiDataId") as? String
        
    }
    //MARK: Archive Methods
     class func archiveFilePath() -> String {
         let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
         return documentsDirectory.appendingPathComponent("UserCountry.archive").path
     }
     
     class func readUserCountryFromArchive() -> [UserCountry]? {
         return NSKeyedUnarchiver.unarchiveObject(withFile: archiveFilePath()) as? [UserCountry]
     }
     
     class func saveUserCountryToArchive(UserCountry: [UserCountry]) -> Bool {
         return NSKeyedArchiver.archiveRootObject(UserCountry, toFile: archiveFilePath())
     }
     
}
