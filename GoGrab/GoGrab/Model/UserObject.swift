//
//  UserObject.swift
//  GrabMyTaxiDriver
//
//  Created by Mac on 11/02/2021.
//


import Foundation
import UIKit

class UserObject{
    
    var myUser: [User]? {didSet {}}
    var myUserCountry: [UserCountry]? {didSet {}}

    
    static let shared = UserObject() //  singleton object
    
    
    func Objresponse(response:[String:Any]){
        let UserObj = response["User"] as! NSDictionary
        let user = User()
        user.Id =  UserObj.value(forKey: "id") as! String
        user.first_name =  UserObj.value(forKey: "first_name") as! String
        user.last_name =  UserObj.value(forKey: "last_name") as! String
        user.email =  UserObj.value(forKey: "email") as! String
        user.phone =  UserObj.value(forKey: "phone") as? String
        user.image =  UserObj.value(forKey: "image") as? String
        user.role =  UserObj.value(forKey: "role") as? String
        user.device_token =  UserObj.value(forKey: "device_token") as? String
        user.token =  UserObj.value(forKey: "token") as? String
        user.active =  UserObj.value(forKey: "active") as! String
        user.country_id =  UserObj.value(forKey: "country_id") as? String
        user.dob =  UserObj.value(forKey: "dob") as? String
        user.gender =  UserObj.value(forKey: "gender") as? String
        user.lat =  UserObj.value(forKey: "lat") as? String
        user.long =  UserObj.value(forKey: "long") as? String
        user.online =  UserObj.value(forKey: "online") as? String
        user.password =  UserObj.value(forKey: "password") as? String
        user.auth_token =  UserObj.value(forKey: "auth_token") as? String
        user.created  = UserObj.value(forKey:  "created") as? String
        user.device  = UserObj.value(forKey:  "device") as? String
        user.ip  = UserObj.value(forKey:  "ip") as? String
        user.phone  = UserObj.value(forKey:  "phone") as? String
        user.social  = UserObj.value(forKey:  "social") as? String
        user.social_id  = UserObj.value(forKey:  "social_id") as? String
        user.username  = UserObj.value(forKey:  "username") as? String
        user.version  = UserObj.value(forKey:  "version") as? String
        user.wallet  = UserObj.value(forKey:  "wallet") as? String
        
        
        self.myUser = [user]
        if User.saveUserToArchive(user: self.myUser!) {
            print("User Saved in Directory")
        }
        let objCountry = response["Country"] as! NSDictionary
        
        let country = UserCountry()
        
        if objCountry.value(forKey: "id")  is NSNull {}else{
            country.id = objCountry.value(forKey: "id") as? String
            country.active = objCountry.value(forKey: "active") as? String
            country.capital = objCountry.value(forKey: "active") as? String
            country.iso = objCountry.value(forKey: "iso") as? String
            country.name = objCountry.value(forKey: "name") as? String
            country.country = objCountry.value(forKey: "country") as? String
            country.iso3 = objCountry.value(forKey: "iso3") as? String
            country.phonecode = objCountry.value(forKey: "phonecode") as? String
            country.country_code = objCountry.value(forKey: "country_code") as? String
            country.currency = objCountry.value(forKey: "currency") as? String
            country.currency_symbol = objCountry.value(forKey: "currency_symbol") as? String
            
            country.emoji = objCountry.value(forKey: "emoji") as? String
            country.emojiU = objCountry.value(forKey: "emojiU") as? String
            country.flag = objCountry.value(forKey: "flag") as? String
            
            country.native = objCountry.value(forKey: "native") as? String
            country.region = objCountry.value(forKey: "region") as? String
            country.short_name = objCountry.value(forKey: "short_name") as? String
            
            country.subregion = objCountry.value(forKey: "subregion") as? String
            country.updated_at = objCountry.value(forKey: "updated_at") as? String
            country.wikiDataId = objCountry.value(forKey: "wikiDataId") as? String
            
            
            self.myUserCountry = [country]
            if UserCountry.saveUserCountryToArchive(UserCountry: self.myUserCountry!){
                print("User Country Saved in Directory")
            }
        }
    }
}
