//
//  ApiHandler.swift
//  HomeX
//
//  Created by Mac on 2019/10/30.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


var BASE_URL = "http://apps.qboxus.com/gograb/"
let API_KEY = "156c4675-9608-4591-b2ec-427503464aac"


let API_BASE_URL = BASE_URL+"api/"

private let SharedInstance = ApiHandler()


enum Endpoint : String {
    case registerUser = "registerUser"
    case login = "login"
    case logout = "logout"
    case verifyPhoneNo  = "verifyPhoneNo"
    case forgotPassword = "forgotPassword"
    case verifyForgotPasswordCode = "verifyForgotPasswordCode"
    case changeEmailAddress = "changeEmailAddress"
    case verifyChangeEmailCode = "verifyChangeEmailCode"
    case changePassword = "changePassword"
    case changePasswordForgot = "changePasswordForgot"
    case showRideTypes = "showRideTypes"
    case addUserPlace = "addUserPlace"
    case deleteUserPlace = "deleteUserPlace"
    case showUserPlaces = "showUserPlaces"
    case verifyCoupon = "verifyCoupon"
    case showUserNotifications = "showUserNotifications"
    case contactUs = "contactUs"
    case deletePaymentCard = "deletePaymentCard"
    case addPaymentCard = "addPaymentCard"
    case showUserCards = "showUserCards"
    case showCountries = "showCountries"
    case showActiveRequest = "showActiveRequest"
    case editProfile = "editProfile"
    case changePhoneNo = "changePhoneNo"
    case rideCancelled = "rideCancelled"
    case showRecentLocations = "showRecentLocations"
    case addRecentLocation = "addRecentLocation"
    case deleteRecentLocation = "deleteRecentLocation"
}
class ApiHandler:NSObject{
    
    var myUser: [User]? {didSet {}}
    var baseApiPath:String!
    var Loder = Loader()
    
    class var sharedInstance : ApiHandler {
        return SharedInstance
    }
    
    override init() {
        self.baseApiPath = API_BASE_URL
    }
    
    //MARK:- Register with email
    
    func registerUserWithEmail(Email:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "email":Email
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.registerUser.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- Register with user
    
    func registerUser(first_name:String,username:String,last_name:String ,Email:String,DOB:String,Password:String,phone:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "email":Email,
            "dob":DOB,
            "password":Password,
            "username":username,
            "first_name":first_name,
            "last_name":last_name,
            "role":"customer",
            "phone":phone
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.registerUser.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    //MARK:- SOCIAL
    
    func registerWithSocial(social:String,social_id:String,auth_token:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "social":social,
            "social_id":social_id,
            "auth_token":auth_token
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.registerUser.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    func registerUserWithSocial(first_name:String,username:String,last_name:String ,Email:String,DOB:String,Password:String,social:String,social_id:String,auth_token:String,role:String,country_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "email":Email,
            "dob":DOB,
            "password":Password,
            "username":username,
            "first_name":first_name,
            "last_name":last_name,
            "social":social,
            "social_id":social_id,
            "auth_token":auth_token,
            "role":"customer",
            "country_id":country_id,
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.registerUser.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:login
    func login(Email:String,Password:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "email":Email,
            "password":Password,
            "role":"customer",
            "country_id":"1"
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.login.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    //MARK:- logout
    func logout(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
       
        parameters = [
            "user_id":user_id,
            
            
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.logout.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- PhoneNumber
    
    func phoneNumber(phone: String,verify: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "phone":phone,
            "verify":verify
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyPhoneNo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- verifyPhoneNumber
    
    func verifyPhoneNumber(phone: String,verify: String,code: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "phone":phone,
            "verify":verify,
            "code":code
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyPhoneNo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:changeEmailAddress
    func changeEmailAddress(email:String,user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "email":email,
            "user_id":user_id
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changeEmailAddress.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    //MARK:verifyChangeEmailCode
    func verifyChangeEmailCode(new_email:String,code:String,user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "new_email":new_email,
            "code":code,
            "user_id":user_id
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyChangeEmailCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:-showCountries
    
    func showCountries(completionHandler:@escaping(  _ result:Bool,  _ responseObject:NSDictionary?)->Void){
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        
        var parameters = [String : String]()
       
        parameters =  [
            "": "",
        ]
        
        let finalUrl =  "\(self.baseApiPath!)\(Endpoint.showCountries.rawValue)"
        
        print(finalUrl)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:-changePhoneNo
    
    func changePhoneNo(user_id:String,phone:String, completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "phone":phone,
            "user_id":user_id
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changePhoneNo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:-forgotPassword
    
    func forgotPassword(email:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
       
        parameters = [
            "email":email
            
            
            
        ]
        var finalUrl = "\(self.baseApiPath!)\(Endpoint.forgotPassword.rawValue)"
        finalUrl = finalUrl.replacingOccurrences(of: " ", with: "%20")
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    //MARK:-verifyforgotPasswordCode
    
    func verifyforgotPasswordCode(email:String,code:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "email":email,
            "code":code
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyForgotPasswordCode.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    //MARK:-changePassword
    
    func changePassword(user_id:String,old_password:String,new_password:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
       
        parameters = [
            "user_id":user_id,
            "old_password":old_password,
            "new_password":new_password
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changePassword.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- changePasswordForgot
    
    func changePasswordForgot(email: String, password: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "email":email,
            "password":password
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.changePasswordForgot.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- showRideTypes
    
    func showRideTypes(pickup_lat: String,pickup_long: String, dropoff_lat: String, dropff_long : String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "pickup_lat":pickup_lat,
            "pickup_long":pickup_long,
            "dropoff_lat":dropoff_lat,
            "dropff_long":dropff_long
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showRideTypes.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- addUserPlace
    
    func addUserPlace(lat: String,long: String, location_string: String, name : String,user_id:String,language_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "lat":lat,
            "long":long,
            "location_string":location_string,
            "name":name,
            "user_id":user_id,
            "language_id":language_id
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addUserPlace.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- deleteUserPlace
    
    func deleteUserPlace(id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "id":id
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.deleteUserPlace.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- showUserPlaces
    
    func showUserPlaces(id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
       
        parameters = [
            "id":id
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserPlaces.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- verifyCoupon
    
    func verifyCoupon(coupon_code: String,user_id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "coupon_code":coupon_code,
            "user_id":user_id
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.verifyCoupon.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- showUserNotifications
    
    func showUserNotifications(user_id: String,starting_point: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id,
            "starting_point":starting_point
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserNotifications.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- contactUs
    
    func contactUs(user_id: String,message: String,attachment: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id,
            "message":message,
            "attachment":attachment
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.contactUs.rawValue)"
        
//        print(finalUrl)
//        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
//                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- deletePaymentCard
    
    func deletePaymentCard(user_id: String,id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id,
            "id":id
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.deletePaymentCard.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- addPaymentCard
    
    func addPaymentCard(user_id: String,strdefault: String,name:String,card:String,cvc:String,exp_month:String,exp_year:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id,
            "default":strdefault ,
            "name":name,
            "card":card,
            "cvc":cvc,
            "exp_month":exp_month,
            "exp_year":exp_year
            
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addPaymentCard.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- showUserCards
    
    func showUserCards(user_id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
      
        parameters = [
            "user_id":user_id,
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showUserCards.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- showActiveRequest
    
    func showActiveRequest(user_id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id,
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showActiveRequest.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
           
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- editProfile
    
    func editProfile(user_id: String,first_name:String,last_name:String,gender:String,dob:String,phone:String,image:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        parameters = [
            "user_id":user_id,
            "first_name":first_name,
            "last_name":last_name,
            "gender":gender,
            "dob":dob,
            "phone":phone,
            "image":image
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.editProfile.rawValue)"
        
//        print(finalUrl)
//        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            //print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                   // print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- editProfile without image
    
    func editProfileWithNoImage(user_id: String,first_name:String,last_name:String,gender:String,dob:String,phone:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id,
            "first_name":first_name,
            "last_name":last_name,
            "gender":gender,
            "dob":dob,
            "phone":phone
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.editProfile.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    
    //MARK:- rideCancelled
    
    func rideCancelled(driver_id: String,reason:String,request_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "driver_id":driver_id,
            "reason":reason,
            "request_id":request_id
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.rideCancelled.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- showRecentLocations
    
    func showRecentLocations(user_id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.showRecentLocations.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    //MARK:- addRecentLocation
    
    func addRecentLocation(user_id: String,lat:String,long:String,short_name:String,location_string:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "user_id":user_id,
            "lat":lat,
            "long":long,
            "short_name":short_name,
            "location_string":location_string
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.addRecentLocation.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
    }
    
    
    //MARK:- deleteRecentLocation
    
    func deleteRecentLocation(id: String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : String]()
        
        parameters = [
            "id":id
            
            
        ]
        let finalUrl = "\(self.baseApiPath!)\(Endpoint.deleteRecentLocation.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            if let json = response.value
            {
                do {
                    let dict = json as? NSDictionary
                    print(dict)
                    completionHandler(true, dict)
                } catch {
                    completionHandler(false, nil)
                }
            }
        }
        
    }
    
}
