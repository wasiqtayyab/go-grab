//
//  User.swift
//  GrabMyTaxiDriver
//
//  Created by mac on 10/01/2021.
//  Copyright © 2021 Dianosoft. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    
    var Id:String?
    var first_name :String?
    var last_name :String?
    var email :String?
    var phone :String?
    var image :String?
    var device_token :String?
    var role : String?
    var token : String?
    var active :String?
    var country_id:String?
    var admin_per_order_commission:String?
    var rider_fee_per_order:String?
    var lat:String?
    var long:String?
    var online:String?
    var password:String?
    
    var auth_token:String?
    var created:String?
    var device:String?
    var dob:String?
    var gender:String?
    var ip:String?
    var social: String?
    var social_id:String?
    var username:String?
    var version:String?
    var wallet:String?
  
   
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
     
        self.Id = aDecoder.decodeObject(forKey: "id") as? String
        self.first_name = aDecoder.decodeObject(forKey: "first_name") as? String
        self.last_name = aDecoder.decodeObject(forKey: "last_name") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.device_token = aDecoder.decodeObject(forKey: "device_token") as? String
        self.role = aDecoder.decodeObject(forKey: "role") as? String
        self.token = aDecoder.decodeObject(forKey: "token") as? String
        self.active = aDecoder.decodeObject(forKey: "active") as? String
        self.country_id = aDecoder.decodeObject(forKey: "country_id") as? String
        self.admin_per_order_commission = aDecoder.decodeObject(forKey: "admin_per_order_commission") as? String
        self.rider_fee_per_order = aDecoder.decodeObject(forKey: "rider_fee_per_order") as? String
        self.lat = aDecoder.decodeObject(forKey: "lat") as? String
        self.long = aDecoder.decodeObject(forKey: "long") as? String
        self.online = aDecoder.decodeObject(forKey: "online") as? String
        self.password = aDecoder.decodeObject(forKey: "password") as? String
        
        self.auth_token = aDecoder.decodeObject(forKey: "auth_token") as? String
        self.created = aDecoder.decodeObject(forKey: "created") as? String
        self.device = aDecoder.decodeObject(forKey: "device") as? String
        self.dob = aDecoder.decodeObject(forKey: "dob") as? String
        self.gender = aDecoder.decodeObject(forKey: "gender") as? String
        self.ip = aDecoder.decodeObject(forKey: "ip") as? String
        self.social = aDecoder.decodeObject(forKey: "social") as? String
        self.social_id = aDecoder.decodeObject(forKey: "social_id") as? String
        self.username = aDecoder.decodeObject(forKey: "username") as? String
        self.version = aDecoder.decodeObject(forKey: "version") as? String
        self.wallet = aDecoder.decodeObject(forKey: "wallet") as? String
       
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.Id, forKey: "id")
        aCoder.encode(self.first_name, forKey: "first_name")
        aCoder.encode(self.last_name, forKey: "last_name")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.image, forKey: "image")
        aCoder.encode(self.device_token, forKey: "device_token")
        aCoder.encode(self.role, forKey: "role")
        aCoder.encode(self.token, forKey: "token")
        aCoder.encode(self.active, forKey: "active")
        aCoder.encode(self.country_id, forKey: "country_id")
        aCoder.encode(self.admin_per_order_commission, forKey: "admin_per_order_commission")
        aCoder.encode(self.rider_fee_per_order, forKey: "rider_fee_per_order")
        aCoder.encode(self.lat, forKey: "lat")
        aCoder.encode(self.long, forKey: "long")
        aCoder.encode(self.online, forKey: "online")
        aCoder.encode(self.password, forKey: "password")
        
        aCoder.encode(self.auth_token, forKey: "auth_token")
        aCoder.encode(self.created, forKey: "created")
        aCoder.encode(self.device, forKey: "device")
        aCoder.encode(self.dob, forKey: "dob")
        aCoder.encode(self.gender, forKey: "gender")
        aCoder.encode(self.ip, forKey: "ip")
        aCoder.encode(self.social, forKey: "social")
        aCoder.encode(self.social_id, forKey: "social_id")
        
        aCoder.encode(self.username, forKey: "username")
        aCoder.encode(self.version, forKey: "version")
        aCoder.encode(self.wallet, forKey: "wallet")
 
    
    }
    //MARK: Archive Methods
    class func archiveFilePath() -> String {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsDirectory.appendingPathComponent("user.archive").path
    }
    
    class func readUserFromArchive() -> [User]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: archiveFilePath()) as? [User]
    }
    
    class func saveUserToArchive(user: [User]) -> Bool {
        return NSKeyedArchiver.archiveRootObject(user, toFile: archiveFilePath())
    }
    
}

