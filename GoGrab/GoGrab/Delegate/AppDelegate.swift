//
//  AppDelegate.swift
//  GoGrab
//
//  Created by Naqash Ali on 19/05/2021.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var myUser: [User]? {didSet {}}
   
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        
      
        
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyBm__ycW-Bx_MWf0cF-lbByVfWptnXNTXM")
        GMSPlacesClient.provideAPIKey("AIzaSyBm__ycW-Bx_MWf0cF-lbByVfWptnXNTXM")
        GIDSignIn.sharedInstance()?.clientID =  FirebaseApp.app()?.options.clientID

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldPlayInputClicks = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        
        self.myUser = User.readUserFromArchive()
                if (myUser != nil && myUser?.count != 0){
                    
                    print("user login")
                    let story = UIStoryboard(name: "Main", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
                                let nav = UINavigationController(rootViewController: vc)
                                nav.navigationBar.isHidden = true
                                window?.rootViewController = nav
                }else {
                    
                    print("user logout")
                    let story = UIStoryboard(name: "Main", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                let nav = UINavigationController(rootViewController: vc)
                                nav.navigationBar.isHidden = true
                                self.window?.rootViewController = nav
                    
                }
        
        
        return true
    }
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {

        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )

    }
    
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

